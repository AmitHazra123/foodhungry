const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const OrderHistorySchema = new Schema({
  items: {
    type: [
      {
        name: {
          type: String,
          required: true
        },
        quantity: {
          type: Number,
          required: true
        },
        price: {
          type: Number,
          required: true
        }
      }
    ],
    required: true
  },
  totalPrice: {
    type: Number,
    required: true
  },
  userDetails: {
    type: {
      email: {
        type: String,
        required: true
      },
      mobile: {
        type: String,
        required: true
      },
      address: {
        type: String,
        required: true
      }
    },
    required: true
  },
  restAdmin: {
    type: {
      restaurantId: {
        type: String,
        required: true
      },
      name: {
        type: String,
        required: true
      },
      address: {
        type: String,
        required: true
      },
      geolocation: {
        type: {
          lat: {
            type: Number,
            required: true
          },
          lng: {
            type: Number,
            required: true
          }
        },
        required: true
      }
    },
    required: true
  },
  status: {
    type: Number,
    required: true
  },
  cancel: {
    type: Boolean
  },
  deliveryPerson: {
    type: {
      mobile: {
        type: Number,
        required: true
      },
      name: {
        type: String,
        required: true
      },
      currentLocation: {
        type: {
          lat: {
            type: Number,
            required: true
          },
          lng: {
            type: Number,
            required: true
          }
        },
        required: true
      }
    }
  },
  paymentMethod: {
    type: String,
    required: true
  },
  orderDate: {
    type: Date,
    default: Date.now
  }
});

module.exports = OrderHistory = mongoose.model(
  "orderhistory",
  OrderHistorySchema
);
