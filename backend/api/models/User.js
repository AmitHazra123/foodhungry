const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  mobile: {
    type: Number,
    required: true
  },
  address: {
    type: String
  },
  location: {
    type: {
      type: String,
      enum: ['Point'],
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  },
  image: {
    type: String,
    default: "https://res.cloudinary.com/amit-hazra/image/upload/v1572948963/xdb4atodqpdlp5js00gq.jpg"
  }
});

module.exports = User = mongoose.model("user", UserSchema);
