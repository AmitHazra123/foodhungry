const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CourierSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  password: {
    type: String,
    required: true
  },
  bikeNo: {
    type: String
  },
  mobile: {
    type: Number,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  location: {
    type: {
      type: String,
      enum: ['Point'],
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  },
  rating: {
    type: Number
  },
  availability: {
    type: Boolean,
    default: false
  },
  image: {
    type: String,
    default: "https://res.cloudinary.com/amit-hazra/image/upload/v1572948963/xdb4atodqpdlp5js00gq.jpg"
  }
});

module.exports = Courier = mongoose.model("courier", CourierSchema);
