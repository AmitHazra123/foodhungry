const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FoodCategorySchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  accountId: {
    type: Schema.Types.ObjectId,
    required: true
  }
});

module.exports = FoodCategory = mongoose.model(
  "foodCategory",
  FoodCategorySchema
);
