const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
  userId: {
    type: Schema.Types.ObjectId,
    required: true
  },
  restaurantId: {
    type: Schema.Types.ObjectId,
    required: true
  },
  courierId: {
    type: Schema.Types.ObjectId
  },
  deliveryAddress: {
    type: String,
    required: true
  },
  orderStatus: {
    type: String,
    required: true
  },
  totalItem: {
    type: Number,
    require: true
  },
  totalPrice: {
    type: Number,
    required: true
  },
  paymentMode: {
    type: String,
    required: true
  },
  paymentStatus: {
    type: String,
    require: true,
  },
  orderDate: {
    type: Date,
    default: Date.now
  },
  items: {
    type: [
      {
        foodId: {
          type: Schema.ObjectId,
          required: true
        },
        name: {
          type: String,
          required: true
        },
        quantity: {
          type: Number,
          required: true
        },
        price: {
          type: Number,
          required: true
        }
      }
    ],
    required: true
  }
});

module.exports = Order = mongoose.model("order", OrderSchema);
