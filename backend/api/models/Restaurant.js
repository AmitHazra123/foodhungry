const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const RestaurantSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  address: {
    type: String,
    required: true
  },
  location: {
    type: {
      type: String,
      enum: ['Point'],
      required: true
    },
    coordinates: {
      type: [Number],
      required: true
    }
  },
  mobile: {
    type: Number,
    required: true
  },
  image: {
    type: String,
    default: "https://res.cloudinary.com/amit-hazra/image/upload/v1572948963/xdb4atodqpdlp5js00gq.jpg"
  },
  rating: {
    type: Number,
    default: 0
  },
  deliveryTime: {
    type: String,
    default: "15-30 mins"
  }
});

RestaurantSchema.index({ location: "2dsphere" });

module.exports = Restaurant = mongoose.model("restaurant", RestaurantSchema);
