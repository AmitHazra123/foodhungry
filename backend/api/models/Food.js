const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const FoodSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  categoryIds: {
    type: [Schema.Types.ObjectId],
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  accountId: {
    type: Schema.Types.ObjectId,
    required: true
  },
  rating: {
    type: Number,
    default: 0
  },
  deliveryTime: {
    type: String,
    required: true
  }
});

module.exports = Food = mongoose.model("food", FoodSchema);
