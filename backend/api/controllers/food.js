// require mongoose
const mongoose = require("mongoose");
const cloudinary = require("cloudinary").v2;

// load model
const Food = require("../models/Food");

// load validation
const validateFoodInput = require("../validation/food");
const isEmpty = require("../validation/is-empty");

// user part

// restaurant admin part

//get all food
exports.getAllFood = (req, res) => {
  Food.find({ accountId: mongoose.Types.ObjectId(req.user._id) }).sort({_id: -1})
    .then(foods => {
      res.status(200);
      res.json(foods);
    })
    .catch(err => {
      res.status(400);
      console.log(err);
    });
};

// get a food
exports.getFood = (req, res) => {
  const foodId = mongoose.Types.ObjectId(req.params.id);
  Food.aggregate([
    {
      $match: {
        _id: foodId
      }
    },
    {
      $lookup: {
        from: 'foodcategories',
        localField: 'categoryIds',
        foreignField: '_id',
        as: 'categories'
      }
    }
  ], (err, response) => {
    if(err) return res.status(400).json(err);
    res.status(200);
    res.json(response[0]);
  });
}

//add a new food
exports.insertFood = (req, res) => {
  
  // error validation
  const food = req.body;
  food.categories = JSON.parse(req.body.categories);
  const {errors, isValid} = validateFoodInput(food);
  if(!isValid || isEmpty(req.files)) {
    if(isEmpty(req.files)) errors.image = "Food image is required";
    return res.status(400).json(errors);
  }

  Food.findOne({ name: food.name })
    .then(async anyFood => {
      if (anyFood) {
        errors.name = "This food already exists";
        return res.status(400).json(errors);
      } else {
        // map category Ids
        const categoryIds = food.categories.map(category => category._id);
        // create new food
        const newFood = new Food({
          name: food.name,
          price: food.price,
          description: food.description,
          deliveryTime: food.deliveryTime,
          accountId: req.user._id,
          categoryIds
        });

        const file = req.files.image;
        // upload file to cloudinary
        await cloudinary.uploader.upload(file.tempFilePath, (err, result) => {
          if (err) throw err;
          newFood.image = result.url;
        });

        newFood
          .save()
          .then(insertedFood => {
            res.status(200);
            res.json(insertedFood);
          })
          .catch(err => {
            res.status(400);
            console.log(err);
          });
      }
    })
    .catch(err => {
      res.status(400);
      console.log(err);
    });
};

// update food by id
exports.updateFood = async (req, res) => {
  // error validation
  const food = req.body;
  food.categories = JSON.parse(req.body.categories);
  const {errors, isValid} = validateFoodInput(food);
  if(!isValid) {
    return res.status(400).json(errors);
  }

  if(!isEmpty(req.files)){
    const file = req.files.image;
    // upload file to cloudinary
    await cloudinary.uploader.upload(file.tempFilePath, (err, result) => {
      if (err) throw err;
      food.image = result.url;
    });
  }

  const foodId = mongoose.Types.ObjectId(req.params.id);
  const categoryIds = food.categories.map(foodCategory => foodCategory._id);

  Food.updateOne(
    { _id: foodId, accountId: req.user._id },
    {
      $set: {
        name: food.name,
        categoryIds,
        price: food.price,
        image: food.image,
        description: food.description,
        accountId: req.user._id, 
        deliveryTime: food.deliveryTime
      }
    }
  )
    .then(updatedFood => {
      return res.status(200).json(updatedFood);
    })
    .catch(err => {
      return res.status(400).json(err);
    });
};

//delete foods
exports.deleteFood = (req, res) => {
  const foodId = mongoose.Types.ObjectId(req.params.id);
  Food.deleteOne({ _id: foodId, accountId: req.user._id })
    .then(deletedFood => {
      res.status(200);
      res.json(deletedFood);
    })
    .catch(err => {
      res.status(400).json(err);
    });
};

// user part
// get all food
exports.getAllFoodByCategories = (req, res) => {
  const restaurantId = mongoose.Types.ObjectId(req.query.restaurantId);
  const categoryId = mongoose.Types.ObjectId(req.query.categoryId);
  Food.find({accountId: restaurantId, categoryIds: {$in: [categoryId]}}).then(foods => {
    res.status(200);
    res.json(foods);
  }).catch(err => {
    res.status(400);
    res.json(err)
  });
}