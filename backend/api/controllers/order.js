//require mongoose
const mongoose = require("mongoose");
// load order model
const Order = require("../models/Order");

// admin part
// to test the route
exports.testOrderRoute = (req, res) => {
  res.json({
    msg: "Successfull"
  });
};

// user part
// place the order
exports.placeOrder = (req, res) => {
    const newOrder = Order({
        userId: req.user._id,
        restaurantId: req.body.restaurantId,
        deliveryAddress: req.body.deliveryAddress,
        orderStatus: req.body.orderStatus,
        totalPrice: req.body.totalPrice,
        paymentMode: req.body.paymentMode,
        paymentStatus: req.body.paymentStatus,
        items: req.body.items
    });
    newOrder.save()
        .then(order =>
            res.status(200).json(order))
        .catch(err =>
            res.status(400).json(err));
}

// uesr can view an upcoming order details
exports.getUpcomingOrder = (req, res) => {
    //mongo code
    const orderId = mongoose.Types.ObjectId(req.params.id);
    Order
        .findOne({userId: req.user._id, _id: orderId})
        .then(upcomingOrder => {
            res.status(200);
            res.json(upcomingOrder);
        })
        .catch(error => {
            res.status(400);
            console.log(error);
        });
}

//restaurant admin part
//restaurant admin can view upcoming order list depending upon status
exports.getUpcomingOrders = (req, res) => {
    // mongo code
    Order.aggregate([
        {
            $match: {
                restaurantId: req.user._id,
                orderStatus: {
                    $nin: [
                        "delivered",
                        "cancelled"
                    ]
                }
            }
        },
        {
            $lookup: {
                from: "users",
                foreignField: "_id",
                localField: "userId",
                as: "userdetails"
            }
        },
        {
            $replaceRoot: {
                newRoot: {
                    $mergeObjects: [
                        {
                            $arrayElemAt: [
                                "$userdetails", 0
                            ]
                        },
                        "$$ROOT"
                    ]
                }
            }
        },
        {
            $project: {
                userId: 0,
                email: 0,
                password: 0,
                address: 0,
                location: 0,
                userdetails: 0
            }
        }
    ], (err, response) => {
        if(!err && response) {
            return res.status(400).json(response[0]);
        }
        return res.status(400).json(err);
    });
}

// restaurant admin can update order status of an upcoming order
exports.updateOrderStatus = (req, res) => {
    const orderId = mongoose.Types.ObjectId(req.params.id);
    Order
        .findOneAndUpdate(
            {restaurantId: req.user._id, _id: orderId},
            {$set: {orderStatus: req.body.orderStatus}},
            {returnOriginal: false}
        )
        .then(order => res.status(200).json(order))
        .catch(err => res.status(400).json(err));
}

// restaurant admin can view cancelled order list
exports.getCancelledOrderList = (req, res) => {
    // mongo code
    Order
        .find({
            restaurantId: req.user._id,
            orderStatus: "cancelled"
        })
        .then(cancelledOrders => {
            res.status(200);
            res.json(cancelledOrders);
        })
        .catch(error => {
            res.status(400);
            console.log(error);
        });
}

// restaurant admin can view cancelled order list
exports.getPendingPaymentOrders = (req, res) => {
    // mongo code
    Order
        .find({
            restaurantId: req.user._id,
            orderStatus: "delivered",
            paymentMode: "cash",
            paymentStatus: "pending"
        })
        .then(cancelledOrders => {
            res.status(200);
            res.json(cancelledOrders);
        })
        .catch(error => {
            res.status(400);
            console.log(error);
        });
}

// restaurant admin can collect cash by update the order payment status
exports.collectCash = (req, res) => {
    const orderId = mongoose.Types.ObjectId(req.params.id);
    Order
        .findOneAndUpdate(
            {restaurantId: req.user._id, _id: orderId},
            {$set: {paymentStatus: req.body.paymentStatus}},
            {returnOriginal: false}
        )
        .then(order => res.status(200).json(order))
        .catch(err => res.status(400).json(err));
}

// restaurant admin can get past order list
exports.getPastOrders = (req, res) => {
    Order
        .find({
            restaurantId: req.user._id,
            orderStatus: "delivered",
            paymentStatus: "paid"
        })
        .then(cancelledOrders => {
            res.status(200);
            res.json(cancelledOrders);
        })
        .catch(error => {
            res.status(400);
            console.log(error);
        });
}