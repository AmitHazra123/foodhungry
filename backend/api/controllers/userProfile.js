const cloudinary = require("cloudinary").v2;

// load model
const User = require("../models/User");

exports.updateUserProfile = async (req, res) => {
  try {
    const userProfile = {
      email: req.body.email,
      name: req.body.name,
      mobile: req.body.mobile,
      address: req.body.address
    };
    let resData = await User.findOneAndUpdate(
      { _id: req.user.id },
      userProfile,
      {
        $new: true 
      }
    );
    res.status(200).json(resData);
  } catch (error) {
    res.status(400).json(error);
  }
};

exports.updateUserProfilePicture = async (req, res, next) => {
  try {
    const file = req.files.image;
    // upload file to cloudinary
    await cloudinary.uploader.upload(file.tempFilePath, async (err, result) => {
      if (err) throw err;
      // update User profile
      const userProfile = {
        image: result.url
      };
      let resData = await User.findOneAndUpdate(
        { _id: req.user.id },
        userProfile,
        {
          $new: true
        }
      );
      res.status(200).json(resData);
    });
  } catch (error) {
    res.status(400).json(error);
  }
};
