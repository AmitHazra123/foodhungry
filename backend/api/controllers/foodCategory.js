const mongoose = require("mongoose");

// load validation module
const isEmpty = require("../validation/is-empty");

// load model
const FoodCategories = require("../models/FoodCategory");
const Food = require("../models/Food");

// get all food category
exports.getAllFoodCategory = (req, res) => {
  FoodCategories.find({ accountId: mongoose.Types.ObjectId(req.user._id)})
    .then(foodCategories => {
      res.status(200);
      res.json(foodCategories);
    })
    .catch(err => {
      res.status(400);
      console.log(err);
    });
};

// insert food category
exports.insertFoodCategory = (req, res) => {

  const errors = {};

  if(isEmpty(req.body.name)) {
    errors.name = "Category name is required"
    return res.status(400).json(errors);
  }

  FoodCategories.findOne({ name: req.body.name })
    .then(anyFoodCategory => {
      if (anyFoodCategory) {
        errors.name = "This food category already exists."
        return res.status(400).json(errors);
      }
      const newFoodCategory = new FoodCategories({
        name: req.body.name,
        description: req.body.description,
        accountId: req.user._id
      });
      newFoodCategory
        .save()
        .then(insertedFoodCategory => {
          res.status(200);
          res.json(insertedFoodCategory);
        })
        .catch(err => {
          res.status(400);
          console.log(err);
        });
    })
    .catch(err => {
      res.status(400);
      console.log(err);
    });
};

// update food category
exports.updateFoodCategory = (req, res) => {
  const foodId = mongoose.Types.ObjectId(req.params.id);
  FoodCategories.findOneAndUpdate(
    { _id: foodId, accountId: req.user._id },
    {
      $set: {
        name: req.body.name,
        description: req.body.description
      }
    },
    { returnOriginal: false }
  )
    .then(updatedFoodCategory => {
      res.status(200);
      res.json(updatedFoodCategory);
    })
    .catch(err => {
      res.status(400);
      console.log(err);
    });
};

// delete food category
exports.deleteFoodCategory = (req, res) => {
  const foodCategoryId = mongoose.Types.ObjectId(req.params.id);
  const restaurantId = mongoose.Types.ObjectId(req.user.id);

  Food.updateMany(
    {accountId: restaurantId, categoryIds: {$in: [foodCategoryId]}},
    {$pull: {categoryIds: {$in: [foodCategoryId]}}}
  ).then(() => {
    FoodCategories.deleteOne({ _id: foodCategoryId, accountId: restaurantId })
      .then(deletedFoodCategory => {
        res.status(200);
        res.json(deletedFoodCategory);
      })
      .catch(err => {
        res.status(400).json(err);
      });
  }).catch(err => res.status(400).json(err));
};
