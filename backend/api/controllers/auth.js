// load models
const User = require("../models/User");
const Restaurant = require("../models/Restaurant");
const Courier = require("../models/Courier");
// const DeliveryPerson = require("../models/Courier");
// const SystemAdmin = require("../models/SystemAdmin");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const axios = require("axios");

// load input validation
const validateRegisterInput = require("../validation/register");
const validateLoginInput = require("../validation/login");

// test the router
exports.test = (req, res) => {
  res.json({ msg: "Success" });
};

// user authentication part
// register a new user
exports.registerUser = (req, res) => {
  // load error data
  const { errors, isValid } = validateRegisterInput(req.body);

  // check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email })
    .then(async user => {
      // check that email id is already exists or not
      if (user) {
        errors.email = "Email already exists";
        return res.status(400).json(errors);
      } else {
        const googleMapApiKey = process.env.FOODHUNGRY_GOOGLE_MAP_API_KEY;
        const encodedLocation = encodeURIComponent(req.body.address);
        const response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?key=${googleMapApiKey}&address=${encodedLocation}`);
        let resultsArray = response.data.results;
        let full_Address = resultsArray[0].formatted_address;
        let latitude = resultsArray[0].geometry.location.lat;
        let longitude = resultsArray[0].geometry.location.lng;
        const location = {
          address: full_Address,
          latitude,
          longitude,
        }
        // construct the new user data
        const userData = new User({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password,
          mobile: req.body.mobile,
          address: location.address,
          location: {type: "Point", coordinates: [location.latitude, location.longitude]}
        });
        // encrypt the password
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(userData.password, salt, (err, hash) => {
            if (err) throw err;
            userData.password = hash;
            userData
              .save()
              .then(user => res.json(user))
              .catch(err => console.log(err));
          });
        });
      }
    })
    .catch(err => console.log(err));
};

// login existing user
exports.loginUser = (req, res) => {
  // load error data
  const { errors, isValid } = validateLoginInput(req.body);

  // check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;
  User.findOne({ email })
    .then(user => {
      // check for the user accross the email id is found or not
      if (!user) {
        // if not found
        errors.email = "No such email exists as a User!";
        return res.status(404).json(errors);
      } else {
        // if found
        // compare the password
        bcrypt.compare(password, user.password).then(isMatch => {
          if (isMatch) {
            // password matched
            // create a payload for jwt token
            const payload = {
              id: user.id,
              name: user.name,
              email: user.email,
              actor: "user"
            };

            // sign on jwt with the payload to create a token
            jwt.sign(
              payload,
              process.env.SECRET_OR_KEY,
              { expiresIn: 43200 },
              (err, token) => {
                if (err) throw err;
                res.json({
                  success: true,
                  token: "Bearer " + token
                });
              }
            );
          } else {
            // password is not matched
            errors.password = "Password is incorrect!";
            return res.status(400).json(errors);
          }
        });
      }
    })
    .catch(err => console.log(err));
};

// get the current user
exports.getCurrentUser = (req, res) => {
  User.aggregate([
    {
      $match: {
        _id: req.user._id,
        email: req.user.email
      }
    },
    {
      $project: {
          name: 1,
          email: 1,
          mobile: 1,
          address: 1,
          image: 1,
          actor: 'user'
      }
    }
  ], (err, response) => {
    if(err) return res.status(400).json(err);
    console.log(response);
    return res.status(200).json(response[0]);
  });
};

// restaurant admin authentication part
// register a new restaurant admin
exports.registerRestaurant = (req, res) => {
  // load error data
  const { errors, isValid } = validateRegisterInput(req.body);

  // check validation
  if (!isValid || req.body.description === "") {
    errors.description = "Description is required";
    return res.status(400).json(errors);
  }

  Restaurant.findOne({ email: req.body.email })
    .then(async admin => {
      // check that email id is already exists or not
      if (admin) {
        errors.email = "Email already exists";
        return res.status(400).json(errors);
      } else {
        const googleMapApiKey = process.env.FOODHUNGRY_GOOGLE_MAP_API_KEY;
        const encodedLocation = encodeURIComponent(req.body.address);
        const response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?key=${googleMapApiKey}&address=${encodedLocation}`);
        let resultsArray = response.data.results;
        let full_Address = resultsArray[0].formatted_address;
        let latitude = resultsArray[0].geometry.location.lat;
        let longitude = resultsArray[0].geometry.location.lng;
        const location = {
          address: full_Address,
          latitude,
          longitude,
        }
        // construct the new user data
        const adminData = new Restaurant({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password,
          mobile: req.body.mobile,
          address: location.address,
          description: req.body.description,
          location: {type: "Point", coordinates: [location.latitude, location.longitude]}
        });

        // encrypt the password
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(adminData.password, salt, (err, hash) => {
            if (err) throw err;
            adminData.password = hash;
            adminData
              .save()
              .then(admin => res.json(admin))
              .catch(err => console.log(err));
          });
        });
      }
    })
    .catch(err => console.log(err));
};

// login existing restaurant admin
exports.loginRestaurant = (req, res) => {
  // load error data
  const { errors, isValid } = validateLoginInput(req.body);

  // check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  Restaurant.findOne({ email })
    .then(admin => {
      if (!admin) {
        // if no such email exists as a Restaurant Admin
        errors.email = "No such email exists as a Restaurant Admin";
        return res.status(404).json(errors);
      } else {
        // if that email id exists
        // compare for the password
        bcrypt.compare(password, admin.password).then(isMatch => {
          if (isMatch) {
            // if password is matched

            // create payload
            const payload = {
              id: admin.id,
              name: admin.name,
              email: admin.email,
              actor: "restadmin"
            };

            // sign to jwt with the payload
            jwt.sign(
              payload,
              process.env.SECRET_OR_KEY,
              { expiresIn: 43200 },
              (err, token) => {
                if (err) throw err;
                res.json({
                  success: true,
                  token: "Bearer " + token
                });
              }
            );
          } else {
            errors.password = "Password is incorrect";
            return res.status(400).json(errors);
          }
        });
      }
    })
    .catch(err => console.log(err));
};

// get the current restaurant admin
exports.getCurrentRestaurant = (req, res) => {

  Restaurant.aggregate([
    {
      $match: {
        _id: req.user._id,
        email: req.user.email
      }
    },
    {
      $project: {
          _id: 1,
          name: 1,
          email: 1,
          description: 1,
          mobile: 1,
          address: 1,
          deliveryTime: 1,
          image: 1,
          actor: 'restadmin'
      }
    }
  ], (err, response) => {
    if(err) return res.status(400).json(err);
    console.log(response);
    return res.status(200).json(response[0]);
  });
  
};

// courier authentication part
// register a new courier
exports.registerCourier = (req, res) => {
  // load error data
  const { errors, isValid } = validateRegisterInput(req.body);

  // check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  Courier.findOne({ email: req.body.email })
    .then(async courier => {
      // check that email id is already exists or not
      if (courier) {
        errors.email = "Email already exists";
        return res.status(400).json(errors);
      } else {
        const googleMapApiKey = process.env.FOODHUNGRY_GOOGLE_MAP_API_KEY;
        const encodedLocation = encodeURIComponent(req.body.address);
        const response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?key=${googleMapApiKey}&address=${encodedLocation}`);
        let resultsArray = response.data.results;
        let full_Address = resultsArray[0].formatted_address;
        let latitude = resultsArray[0].geometry.location.lat;
        let longitude = resultsArray[0].geometry.location.lng;
        const location = {
          address: full_Address,
          latitude,
          longitude,
        }
        // construct the new user data
        const courierData = new Courier({
          name: req.body.name,
          email: req.body.email,
          password: req.body.password,
          mobile: req.body.mobile,
          address: location.address, 
          location: {type: "Point", coordinates: [location.latitude, location.longitude]}
        });

        // encrypt the password
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(courierData.password, salt, (err, hash) => {
            if (err) throw err;
            courierData.password = hash;
            courierData
              .save()
              .then(courier => res.json(courier))
              .catch(err => console.log(err));
          });
        });
      }
    })
    .catch(err => console.log(err));
};

// login existing courier
exports.loginCourier = (req, res) => {
  // load error data
  const { errors, isValid } = validateLoginInput(req.body);

  // check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;

  Courier.findOne({ email })
    .then(courier => {
      if (!courier) {
        // if no such email exists as a Restaurant Admin
        errors.email = "No such email exists as a Courier";
        return res.status(404).json(errors);
      } else {
        // if that email id exists
        // compare for the password
        bcrypt.compare(password, courier.password).then(isMatch => {
          if (isMatch) {
            // if password is matched

            // create payload
            const payload = {
              id: courier.id,
              name: courier.name,
              email: courier.email,
              actor: "courier"
            };

            // sign to jwt with the payload
            jwt.sign(
              payload,
              process.env.SECRET_OR_KEY,
              { expiresIn: 43200 },
              (err, token) => {
                if (err) throw err;
                res.json({
                  success: true,
                  token: "Bearer " + token
                });
              }
            );
          } else {
            errors.password = "Password is incorrect";
            return res.status(400).json(errors);
          }
        });
      }
    })
    .catch(err => console.log(err));
};

// get the current courier
exports.getCurrentCourier = (req, res) => {
  
  Courier.aggregate([
    {
      $match: {
        _id: req.user._id,
        email: req.user.email
      }
    },
    {
      $project: {
          _id: 1,
          name: 1,
          email: 1,
          mobile: 1,
          bikeNo: 1,
          address: 1,
          image: 1,
          actor: 'courier'
      }
    }
  ], (err, response) => {
    if(err) return res.status(400).json(err);
    return res.status(200).json(response[0]);
  });
};

// function to get latitude and logitude
const getLocation = async (location) => {
  const googleMapApiKey = process.env.FOODHUNGRY_GOOGLE_MAP_API_KEY;

    const response = await axios.get(`https://maps.googleapis.com/maps/api/geocode/json?key=${googleMapApiKey}&address=${location}`);
    let resultsArray = response.data.results;
    let full_Address = resultsArray[0].formatted_address;
    let latitude = resultsArray[0].geometry.location.lat;
    let longitude = resultsArray[0].geometry.location.lng;
    const locationDetails = {
      address: full_Address,
      latitude,
      longitude,
    }
    return locationDetails;

}