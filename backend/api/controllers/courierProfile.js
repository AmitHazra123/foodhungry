const cloudinary = require("cloudinary").v2;

// load model
const Courier = require("../models/Courier");

// update profile
exports.updateCourierProfile = async (req, res) => {
    try {
      const courierProfile = {
        email: req.body.email,
        name: req.body.name,
        bikeNo: req.body.bikeNo,
        mobile: req.body.mobile,
        address: req.body.address
      };
      let resData = await Courier.findOneAndUpdate(
        { _id: req.user.id },
        courierProfile,
        {
          $new: true 
        }
      );
      console.log(resData);
      res.status(200).json(resData);
    } catch (error) {
      res.status(400).json(error);
    }
  };

// update profile picture
exports.updateCourierProfilePicture = async (req, res, next) => {
    try {
      const file = req.files.image;
      // upload file to cloudinary
      await cloudinary.uploader.upload(file.tempFilePath, async (err, result) => {
        if (err) throw err;
        // update User profile
        const courierProfile = {
          image: result.url
        };
        let resData = await Courier.findOneAndUpdate(
          { _id: req.user.id },
          courierProfile,
          {
            $new: true
          }
        );
        res.status(200).json(resData);
      });
    } catch (error) {
      res.status(400).json(error);
    }
  };

// delete profile
