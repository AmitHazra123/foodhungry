// require library module
const mongoose = require("mongoose");
// load models
const OrderHistory = require("../models/OrderHistory");

// restaurant admin part
//Restaurant Admin can get a past order details by order id 
exports.getPastOrderDetails = (req, res) => {
    //mongo code
    const orderId = mongoose.Types.ObjectId(req.params.id);
    OrderHistory
        .find({_id: orderId, accountId: req.user.id})
        .then(pastOrderDetails => {
            res.status(200);
            res.json(pastOrderDetails);
        })
        .catch(error => {
            res.status(400);
            console.log(error);
        });
}

//Restaurant Admin can get past order list from OrderHistory
exports.getPastOrderList = (req, res) => {
    OrderHistory
    .find({accountId: req.user.id})
    .then(pastOrderList => {
        res.status(200);
        res.json(pastOrderList);
    })
    .catch(error => {
        res.status(400);
        console.log(error);
    });
}
