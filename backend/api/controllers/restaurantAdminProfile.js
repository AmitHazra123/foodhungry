const cloudinary = require("cloudinary").v2;

// load model
const Users = require("../models/User");
const Restaurant = require("../models/Restaurant");

exports.updateRestaurantProfile = async (req, res) => {
  try {
    const adminProfile = {
      email: req.body.email,
      name: req.body.name,
      mobile: req.body.mobile,
      address: req.body.address,
      deliveryTime: req.body.deliveryTime,
      description: req.body.description
    };
    let resData = await Restaurant.findOneAndUpdate(
      { _id: req.user.id },
      adminProfile,
      {
        $new: true
      }
    );
    res.json(resData);
  } catch (error) {
    console.error(error);
  }
};

exports.updateRestaurantProfilePicture = async (req, res, next) => {
  try {
    const file = req.files.image;
    // upload file to cloudinary
    await cloudinary.uploader.upload(file.tempFilePath, async (err, result) => {
      if (err) throw err;
      // update restaurant admin profile
      const adminProfile = {
        image: result.url
      };
      let resData = await Restaurant.findOneAndUpdate(
        { _id: req.user.id },
        adminProfile,
        {
          $new: true
        }
      );
      res.json(resData);
    });
  } catch (error) {
    console.error(error);
  }
};
