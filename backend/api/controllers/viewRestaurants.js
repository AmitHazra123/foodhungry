const mongoose = require("mongoose");

// load models
const Restaurant = require("../models/Restaurant");

// user part
// user can view nearest restaurants based on location
exports.restaurantsWithNearestLocation = (req, res) => {
  const longitude = req.body.longitude.toString();
  const latitude = req.body.latitude.toString();
  console.log(longitude, latitude);
  Restaurant.find({
    location: {
      $near: {
        $geometry: {
          type: "Point",
          coordinates: [latitude, longitude],
        },
        $minDistance: 7000,
      },
    },
  }, {
    _id: 1,
    name: 1,
    address: 1,
    description: 1,
    deliveryTime: 1,
    rating: 1,
    location: 1,
    image: 1
  })
    .then((restaurants) => {
      res.status(200);
      res.json(restaurants);
    })
    .catch((err) => {
      res.status(400);
      console.log(err);
    });
};

// user can search restaurants based on different foods or restaurants
exports.searchRestaurants = (req, res) => {};

// get a restaurant
exports.getRestaurant = (req, res) => {
  const restaurantId = mongoose.Types.ObjectId(req.params.id);

  Restaurant.aggregate(
    [
      {
        $match: { _id: restaurantId },
      },
      {
        $lookup: {
          from: "foodcategories",
          localField: "_id",
          foreignField: "accountId",
          as: "categories",
        },
      },
    ],
    (err, response) => {
      if (!err) {
        res.status(200);
        return res.json(response[0]);
      }
      res.status(400);
      return res.json(err);
    }
  );
};
