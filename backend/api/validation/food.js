const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateFoodInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.price = !isEmpty(data.price) ? data.price : "";
  data.description = !isEmpty(data.description) ? data.description : "";
  data.deliveryTime = !isEmpty(data.deliveryTime) ? data.deliveryTime : "";

  if (Validator.isEmpty(data.name)) {
    errors.name = "Food name field is required";
  }

  if (isEmpty(data.categories)) {
    errors.categories = "Food Categories field is required";
  }

  if (Validator.isEmpty(data.price)) {
    errors.price = "Price field is required";
  }

  if (Validator.isEmpty(data.description)) {
    errors.description = "Description field is required";
  }

  if (Validator.isEmpty(data.deliveryTime)) {
    errors.deliveryTime = "Delivery time field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
