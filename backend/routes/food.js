const express = require("express");
const router = express.Router();
const passport = require("passport");

// load controllers
const {
  getAllFood,
  getFood,
  insertFood,
  updateFood,
  deleteFood,
  getAllFoodByCategories
} = require("../api/controllers/food");

// user part
// user can view a perticular restaurant foods details by food ids and restaurant ids
// user can view a perticular restaurant foods details by only food ids
// user can search by a perticular food with its tagname and foodname

// restaurant admin part

// restaurant admin can get all food
// @method           GET
// @description      to get all food
// @link             /api/food/restadmin/getallfood
router.get(
  "/restadmin/getallfood",
  passport.authenticate("restadmin", { session: false }),
  getAllFood
);

// restaurant admin can get a food
// @method           GET
// @description      to get a food
// @link             /api/food/restadmin/getfood
router.get(
  "/restadmin/getfood/:id",
  passport.authenticate("restadmin", { session: false }),
  getFood
);

// restaurant admin can add a food
// @method           POST
// @description      to insert a food
// @link             /api/food/restadmin/addfood
router.post(
  "/restadmin/insertfood",
  passport.authenticate("restadmin", { session: false }),
  insertFood
);

// restaurant admin can add a food
// @method           PUT
// @description      to update a food
// @link             /api/food/restadmin/updatefood/:id
router.put(
  "/restadmin/updatefood/:id",
  passport.authenticate("restadmin", { session: false }),
  updateFood
);

// restaurant admin can delete food on the food array of ids and if its blank after delete then also remove the food details
// @method           DELETE
// @description      to delete a food
// @link             /api/food/restadmin/deletefood
router.delete(
  "/restadmin/deletefood/:id",
  passport.authenticate("restadmin", { session: false }),
  deleteFood
);

// user can get all food
// @method           GET
// @description      to gel all food
// @link             /api/food/user/getallfoodbycategories
router.get(
  "/user/getallfoodbycategories",
  getAllFoodByCategories
);


module.exports = router;
