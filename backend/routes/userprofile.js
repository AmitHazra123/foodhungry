const express = require("express");
const router = express.Router();
const passport = require("passport");

// load controllers
const {
  updateUserProfile,
  updateUserProfilePicture
} = require("../api/controllers/userProfile");

// user own tasks (Private)
// users can edit profile
// users can delete profile

// restaurant admin own tasks (Private)

// restaurant admin edit profile
router.put(
  "/user/update-profile",
  passport.authenticate("user", { session: false }),
  updateUserProfile
);

// restaurant admin profile picture update
router.put(
  "/user/update-profile-picture",
  passport.authenticate("user", { session: false }),
  updateUserProfilePicture
);

// restaurant admin can delete profile

module.exports = router;
