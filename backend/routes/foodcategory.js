const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");

// load controllers
const {
  insertFoodCategory,
  updateFoodCategory,
  deleteFoodCategory,
  getAllFoodCategory
} = require("../api/controllers/foodCategory");

// user part
// user can view different food categories depending upon restaurant food categories ids

// restaurant admin part
// restaurant admin can get all food category
router.get(
  "/restadmin/getallfoodcategory",
  passport.authenticate("restadmin", { session: false }),
  getAllFoodCategory
);

// restaurant admin can add food category by its id
router.post(
  "/restadmin/insertfoodcategory",
  passport.authenticate("restadmin", { session: false }),
  insertFoodCategory
);

// restaurant admin can update food category by its id
router.put(
  "/restadmin/updatefoodcategory/:id",
  passport.authenticate("restadmin", { session: false }),
  updateFoodCategory
);

// restaurant admin can delete food category so that restaurant ids are deleted from the restaurant ids array of food model and then food category and delete food on item also
router.delete(
  "/restadmin/deletefoodcategory/:id",
  passport.authenticate("restadmin", { session: false }),
  deleteFoodCategory
);
module.exports = router;
