const express = require("express");
const passport = require("passport");
const router = express.Router();
const mongoose = require("mongoose");

// load controllers
const {updateCourierProfile, updateCourierProfilePicture} = require("../api/controllers/courierProfile");

// delivery person can edit their profiles
router.put(
    "/courier/update-profile",
    passport.authenticate("courier", {session: false}),
    updateCourierProfile
); 

// delivery person can update their profile picture
router.put(
    "/courier/update-profile-picture",
    passport.authenticate("courier", { session: false }),
    updateCourierProfilePicture
  );

// user part
// user can rate the delivery person profiles

module.exports = router;
