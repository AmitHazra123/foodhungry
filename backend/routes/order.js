const express = require("express");
const router = express.Router();
const passport = require("passport");

// load controllers
const {
  testOrderRoute,
  placeOrder,
  getUpcomingOrders,
  getUpcomingOrder,
  getCancelledOrderList,
  updateOrderStatus,
  getPendingPaymentOrders,
  getPastOrders,
  collectCash
} = require("../api/controllers/order");

// test
router.get("/test", testOrderRoute);

// user part
// user can place order
router.post(
  "/user/placeorder",
  passport.authenticate("user", { session: false }),
  placeOrder
);
// user can view upcoming orders depending upon order date and status
// user can view past order list depending upon order status and order date
// user can cancel order
// restaurant admin can view an upcoming order details
router.get(
  "/user/getupcomingorder/:id",
  passport.authenticate('restadmin', {session:false}),
  getUpcomingOrder
);

// **user can place  order and then there should be a code for checking the nearest available delivery boy and then try to assign if accepted then assign to orders

// restaurant admin part
// restaurant admin can view upcoming order list depending upon order date and status
router.get(
    "/restadmin/getupcomingorders",
    passport.authenticate('restadmin', {session:false}),
    getUpcomingOrders
);
// restaurant admin can view cancelled orders
router.get(
  "/restadmin/getcancelledorders",
  passport.authenticate('restadmin', {session:false}),
  getCancelledOrderList
);
// restaurant admin can view cancelled orders
router.get(
  "/restadmin/getpendingpaymentorders",
  passport.authenticate('restadmin', {session:false}),
  getPendingPaymentOrders
);
// restaurant admin can view past orders
router.get(
  "/restadmin/getpastorders",
  passport.authenticate('restadmin', {session: false}),
  getPastOrders
);
// restaurant admin can change order status like preparing order and handover order
router.put(
  "/restadmin/updateorderstatus/:id",
  passport.authenticate("restadmin", {session: false}),
  updateOrderStatus
);
// restaurant admin can change order status like preparing order and handover order
router.put(
  "/restadmin/collectcash/:id",
  passport.authenticate("restadmin", {session: false}),
  collectCash
);

// restaurant admin can initiate refund request

module.exports = router;
