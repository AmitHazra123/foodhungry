const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");

// load controllers
const {restaurantsWithNearestLocation, searchRestaurants, getRestaurant} = require("../api/controllers/viewRestaurants");

// user part
// user can view nearest restaurants based on location
router.post("/location", restaurantsWithNearestLocation);
// user can search restaurants based on different foods or restaurants
router.post("/search", searchRestaurants);
// get a restaurant
router.get("/:id", getRestaurant);

module.exports = router;
