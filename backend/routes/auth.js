// require tools
const express = require("express");
const router = express.Router();
const passport = require("passport");

// load controllers
const {
  test,
  registerUser,
  loginUser,
  getCurrentUser,
  registerRestaurant,
  loginRestaurant,
  getCurrentRestaurant,
  registerCourier,
  loginCourier,
  getCurrentCourier
} = require("../api/controllers/auth");

// authentication tasks

// test router
router.get("/test", test);

// user part

// user can register
router.post("/user/register", registerUser);

// user can login
router.post("/user/login", loginUser);

// return current user
router.get(
  "/user/current",
  passport.authenticate("user", { session: false }),
  getCurrentUser
);

// restaurant admin part

// restaurant admin can register
router.post("/restadmin/register", registerRestaurant);

// restaurant admin can login
router.post("/restadmin/login", loginRestaurant);

// return current restaurant admin
router.get(
  "/restadmin/current",
  passport.authenticate("restadmin", { session: false }),
  getCurrentRestaurant
);

// delivery person part
// restaurant admin can register
router.post("/courier/register", registerCourier);

// restaurant admin can login
router.post("/courier/login", loginCourier);

// return current restaurant admin
router.get(
  "/courier/current",
  passport.authenticate("courier", { session: false }),
  getCurrentCourier
);

// system admin part
// system admin can register
// system admin can login

module.exports = router;
