const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const passport = require("passport");
//load controllers
const {getPastOrderDetails, getPastOrderList} = require("../api/controllers/orderHistory");

// user part
// user can view past orders

//admin part
//Restaurant Admin can get a past order details by order id
router.get(
    "/restadmin/getpastorderdetails/:id",
    passport.authenticate('restadmin', {session:false}),
    getPastOrderDetails
);
//Restaurant Admin can get past order list from OrderHistory
router.get(
    "/restadmin/getpastorderlist",
    passport.authenticate('restadmin', {session:false}),
    getPastOrderList
);



module.exports = router;
