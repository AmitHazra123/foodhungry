const express = require("express");
const router = express.Router();
const passport = require("passport");

// load controllers
const {
  updateRestaurantProfile,
  updateRestaurantProfilePicture
} = require("../api/controllers/restaurantAdminProfile");

// user own tasks (Private)
// users can edit profile
// users can delete profile

// restaurant admin own tasks (Private)

// restaurant admin edit profile
router.put(
  "/restadmin/update-profile",
  passport.authenticate("restadmin", { session: false }),
  updateRestaurantProfile
);

// restaurant admin profile picture update
router.put(
  "/restadmin/update-profile-picture",
  passport.authenticate("restadmin", { session: false }),
  updateRestaurantProfilePicture
);

// restaurant admin can delete profile

module.exports = router;
