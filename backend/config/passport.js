const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const mongoose = require("mongoose");
const User = mongoose.model("user");
const Restaurant = mongoose.model("restaurant");
const Courier = mongoose.model("courier");

const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.SECRET_OR_KEY;

module.exports = passport => {
  passport.use(
    "user",
    new JwtStrategy(opts, (jwt_payload, done) => {
      User.findById(jwt_payload.id)
        .then(user => {
          if (user) {
            return done(null, user);
          }
          return done(null, false);
        })
        .catch(err => console.log(err));
    })
  );
  passport.use(
    "restadmin",
    new JwtStrategy(opts, (jwt_payload, done) => {
      Restaurant.findById(jwt_payload.id)
        .then(admin => {
          if (admin) {
            return done(null, admin);
          }
          return done(null, false);
        })
        .catch(err => console.log(err));
    })
  );
  passport.use(
    "courier",
    new JwtStrategy(opts, (jwt_payload, done) => {
      Courier.findById(jwt_payload.id)
        .then(courier => {
          if (courier) {
            return done(null, courier);
          }
          return done(null, false);
        })
        .catch(err => console.log(err));
    })
  );
};
