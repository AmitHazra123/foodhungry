const cloudinary = require("cloudinary").v2;

const connectCloudinary = async () => {
  await cloudinary.config({
    cloud_name: process.env.FOODHUNGRY_CLOUD_NAME,
    api_key: process.env.FOODHUNGRY_API_KEY,
    api_secret: process.env.FOODHUNGRY_API_SECRET
  });
};

module.exports = connectCloudinary;
