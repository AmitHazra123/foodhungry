const express = require("express");
const fileUpload = require("express-fileupload");
const dotenv = require("dotenv");
const bodyParser = require("body-parser");
const passport = require("passport");
const path = require("path");
const connectDB = require("./config/db");
const cors = require("cors");
const connectCloudinary = require("./config/cloudinary");

// require routes
const auth = require("./routes/auth");
const deliveryPersonDuty = require("./routes/deliverypersonduty");
const courierProfile = require("./routes/courierProfile");
const food = require("./routes/food");
const foodCategory = require("./routes/foodcategory");
const locationTracking = require("./routes/locationtracking");
const viewRestaurants = require("./routes/viewRestaurants");
const order = require("./routes/order");
const orderHistory = require("./routes/orderhistory");
const payment = require("./routes/payment");
const restaurantAdminProfile = require("./routes/restadminprofile");
const systemAdmin = require("./routes/sysadmin");
const userProfile = require("./routes/userprofile");
const verification = require("./routes/verification");

express.static(path.join(__dirname, "../frontenduser/build"));

// load env vars
dotenv.config({ path: "./config/config.env" });

// connect to Database
connectDB();

// create app
const app = express();

//passport middleware
app.use(passport.initialize());

//passport Config
require("./config/passport.js")(passport);

//Body parser Middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use file upload
app.use(fileUpload({ useTempFiles: true }));
app.use(cors());

// cloudinary configuration
connectCloudinary();

// use routes
app.use("/api/auth", auth);
app.use("/api/deliveryPersonDuty", deliveryPersonDuty);
app.use("/api/courierProfile", courierProfile);
app.use("/api/food", food);
app.use("/api/foodCategory", foodCategory);
app.use("/api/locationTracking", locationTracking);
app.use("/api/viewrestaurants", viewRestaurants);
app.use("/api/order", order);
app.use("/api/orderHistory", orderHistory);
app.use("/api/payment", payment);
app.use("/api/restaurantAdminProfile", restaurantAdminProfile);
app.use("/api/systemAdmin", systemAdmin);
app.use("/api/userProfile", userProfile);
app.use("/api/verification", verification);

// express.static(path.join(__dirname, "../frontenduser/build"));
//   console.log(process.env.NODE_ENV);
//   app.get("/", (req, res) => {
//     res.json({msg: 'hello'});
//     res.sendFile(path.resolve(__dirname, "../frontenduser", "build", "index.html"));
//   });

// production level code
const env = process.env.NODE_ENV;
if (env.localeCompare("production") == 1) {
  app.get("/", (req, res) => {
    res.sendFile(
      path.resolve(__dirname, "../frontenduser", "build", "index.html")
    );
  });
}

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));
