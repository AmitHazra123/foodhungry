import React from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import { makeStyles, createMuiTheme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import styles from "assets/jss/user-kit/views/loginPage";
import { primaryColor } from "assets/jss/user-kit";
import { ThemeProvider } from "@material-ui/styles";

import {
  Grid,
  Typography,
  Paper,
  Button,
  Avatar,
  Stepper,
  Step,
  StepLabel
} from "@material-ui/core";

import FoodCart from "components/UserComponents/FoodCart/FoodCart";

const useStyles = makeStyles(styles);

function Orders(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [activeStep, setActiveStep] = React.useState(1);
  const [skipped, setSkipped] = React.useState(new Set());
  const [accept, setAccept] = React.useState(false);

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: primaryColor
      },
      secondary: {
        main: "#f44336"
      }
    }
  });

  const isStepOptional = step => {
    return step === 1;
  };

  const isStepSkipped = step => {
    return skipped.has(step);
  };

  function getSteps() {
    return ["Order Accepted", "Preparing", "Picked Up", "Food Delivered"];
  }

  const handleNext = () => {
    let newSkipped = skipped;
    if (isStepSkipped(activeStep)) {
      newSkipped = new Set(newSkipped.values());
      newSkipped.delete(activeStep);
    }

    setActiveStep(prevActiveStep => prevActiveStep + 1);
    setSkipped(newSkipped);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const steps = getSteps();

  return (
    <ThemeProvider theme={theme}>
      <div className={classes.root}>
        <div style={{ overflow: "hidden" }}>
          <Grid container lg={12} md={12} spacing={2}>
            <Grid container item lg={12} md={12}>
              <Paper
                className={classes.root}
                style={{ background: "#F5F5F5", width: theme.spacing(140) }}
              >
                <Grid container lg={12} md={12}>
                  <Grid item lg={3} md={3}>
                    <Avatar
                      src="https://res.cloudinary.com/amit-hazra/image/upload/v1573236974/anjg3omlrzfx0i58rvqe.jpg"
                      alt="Amit Hazra"
                      style={{
                        width: theme.spacing(20),
                        height: theme.spacing(20),
                        marginTop: 40
                      }}
                    />
                  </Grid>
                  <Grid item lg={7} md={6}>
                    <Typography variant="h4">User Name</Typography>
                    <Typography variant="subtitle1">
                      Kundallahali, Whitefield, Bengaluru
                    </Typography>
                    <Typography variant="subtitle2">
                      9093079136
                    </Typography>
                    <Typography variant="subtitle2">
                      1 item for ₹ 130.00 • 27 Sep at 02:46 pm •{" "}
                      <Button color="primary">View Receipt</Button>
                    </Typography>
                    <Typography variant="subtitle2">
                      1. Non Veg Executive Thali
                    </Typography>
                    <br />
                    <Typography variant="h4">Courier Name</Typography>
                    <Typography variant="subtitle1">
                      Kundallahali, Whitefield, Bengaluru
                    </Typography>
                    <Typography variant="subtitle2">
                      9093079136
                    </Typography>
                    <br />
                    <Stepper
                      activeStep={activeStep}
                      style={{ display: accept ? "flex" : "none" }}
                    >
                      {steps.map((label, index) => {
                        return (
                          <Step key={label}>
                            <StepLabel>{label}</StepLabel>
                          </Step>
                        );
                      })}
                    </Stepper>
                    <br />
                    <Button
                      style={{ display: accept ? "inline" : "none" }}
                      disabled={activeStep === 0}
                      onClick={handleBack}
                    >
                      Back
                    </Button>
                    <Button
                      style={{ display: accept ? "inline" : "none" }}
                      color="primary"
                      onClick={handleNext}
                    >
                      {activeStep === steps.length - 1 ? "Finish" : "Next"}
                    </Button>
                    <Button
                      style={{ display: !accept ? "inline" : "none" }}
                      color="primary"
                      variant="contained"
                      onClick={() => setAccept(true)}
                    >
                      Accept
                    </Button>{" "}
                    <Button
                      style={{ display: !accept ? "inline" : "none" }}
                      color="secondary"
                      variant="contained"
                    >
                      Reject
                    </Button>
                  </Grid>
                  <Grid item lg={2} md={3}>
                    <Avatar
                      src="https://res.cloudinary.com/amit-hazra/image/upload/v1573236974/anjg3omlrzfx0i58rvqe.jpg"
                      alt="Amit Hazra"
                      style={{
                        width: theme.spacing(20),
                        height: theme.spacing(20),
                        marginTop: 40
                      }}
                    />
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Grid>
        </div>
      </div>
    </ThemeProvider>
  );
}

export default withRouter(Orders);
