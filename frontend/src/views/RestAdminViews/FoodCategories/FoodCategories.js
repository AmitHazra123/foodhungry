import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/RestAdminComponents/Grid/GridItem.js";
import GridContainer from "components/RestAdminComponents/Grid/GridContainer.js";
import Table from "components/RestAdminComponents/Table/Table.js";
import Card from "components/RestAdminComponents/Card/Card.js";
import CardHeader from "components/RestAdminComponents/Card/CardHeader.js";
import CardBody from "components/RestAdminComponents/Card/CardBody.js";
import CustomInput from "components/RestAdminComponents/CustomInput/CustomInput";
import Button from "components/RestAdminComponents/CustomButtons/Button.js";

import { IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Create";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";

// acitons
import {
  getAllFoodCategory,
  addFoodCategory,
  updateFoodCategory,
  deleteFoodCategory
} from "actions/foodCategoryAction";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

function TableList(props) {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");

  const [updateNameField, setUpdateNameField] = useState("");
  const [updateDescriptionField, setUpdateDescriptionField] = useState("");

  const [error, setError] = useState({});
  const [updateId, setUpdateId] = useState("");

  useEffect(() => {
    props.getAllFoodCategory();
  }, []);

  useEffect(() => {
    setError(props.errors.error);
    cancelUpdateFoodCategory();
  }, [props])

  const classes = useStyles();

  function addFoodCategory() {
    const newFoodCategory = {
      name,
      description
    };
    props.addFoodCategory(newFoodCategory);
    setName("");
    setDescription("");
  }

  function deleteFoodCategory(foodCategoryId) {
    props.deleteFoodCategory(foodCategoryId);
  }

  function setUpdateFoodCategory(foodCategory) {
    setUpdateId(foodCategory._id);
    setUpdateNameField(foodCategory.name);
    setUpdateDescriptionField(foodCategory.description);
  }

  function cancelUpdateFoodCategory() {
    setUpdateId("");
    setUpdateNameField("");
    setUpdateDescriptionField("");
  }

  function updateFoodCategory(foodCategoryId) {
    // update food category
    const updateFoodCategoryObj = {
      name: updateNameField,
      description: updateDescriptionField
    }
    props.updateFoodCategory(foodCategoryId, updateFoodCategoryObj);
  }

  const { foodCategories } = props;

  let foodCategoriesTableData = [];
  if (Object.keys(foodCategories).length > 0) {
    foodCategoriesTableData = foodCategories.map(foodCategory => {
      let nameComponent = updateId !== foodCategory._id ? foodCategory.name : (
        <CustomInput
          labelText="Food Category Name"
          id="food-category-name"
          formControlProps={{
            fullWidth: true,
            onChange: e => setUpdateNameField(e.target.value)
          }}
          error={error.name}
          value={updateNameField}
        />
      );
      let descriptionComponent = updateId !== foodCategory._id ? foodCategory.description : (
        <CustomInput
          labelText="Description"
          id="food-category-description"
          formControlProps={{
            fullWidth: true,
            onChange: e => setUpdateDescriptionField(e.target.value)
          }}
          value={updateDescriptionField}
        />
      );
      let iconComponents = updateId !== foodCategory._id ? (
        <IconButton onClick={() => setUpdateFoodCategory(foodCategory)} aria-label="upload picture" component="span">
          <EditIcon />
        </IconButton>
      ) : (
        <GridContainer>
          <GridItem>
            <br/>
            <IconButton onClick={() => updateFoodCategory(foodCategory._id)} aria-label="upload picture" component="span">
              <CheckIcon />
            </IconButton>
          </GridItem>
          <GridItem>
            <br/>
            <IconButton onClick={() => cancelUpdateFoodCategory()} aria-label="upload picture" component="span">
              <CloseIcon />
            </IconButton>
          </GridItem>
        </GridContainer>
      );

      return [
        nameComponent,
        descriptionComponent,
        iconComponents,
        (
          <IconButton onClick={() => deleteFoodCategory(foodCategory._id)} color="secondary" aria-label="upload picture" component="span">
            <DeleteIcon />
          </IconButton>
        )
      ];
    });
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card plain>
          <CardHeader plain color="primary">
            <h4 className={classes.cardTitleWhite}>Add Food Category</h4>
            <p className={classes.cardCategoryWhite}>
              Add Food Category to your menu
            </p>
          </CardHeader>
          <CardBody>
            <GridContainer>
              <GridItem xs={12} sm={12} md={4}>
                <CustomInput
                  labelText="Food Category Name"
                  id="food-category-name"
                  formControlProps={{
                    fullWidth: true,
                    onChange: e => setName(e.target.value)
                  }}
                  error={error.name}
                  value={name}
                />
                {error.name ? (<span style={{color: "red"}}>{error.name}</span>) : ""}
              </GridItem>
              <GridItem xs={12} sm={12} md={6}>
                <CustomInput
                  labelText="Description"
                  id="food-category-description"
                  formControlProps={{
                    fullWidth: true,
                    onChange: e => setDescription(e.target.value)
                  }}
                  value={description}
                />
              </GridItem>
              <GridItem xs={12} sm={12} md={2}>
                <br/>
                <Button color="primary" onClick={addFoodCategory}>
                  Add
                </Button>
              </GridItem>
            </GridContainer>
          </CardBody>
        </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Food Category</h4>
            <p className={classes.cardCategoryWhite}>
              Here is the details of Food Categories
            </p>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["Category Name", "Description", "Edit", "Delete"]}
              tableData={foodCategoriesTableData}
            />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  );
}

TableList.propTypes = {
  errors: PropTypes.object.isRequired,
  foodCategories: PropTypes.object.isRequired,
  getAllFoodCategory: PropTypes.func.isRequired,
  addFoodCategory: PropTypes.func.isRequired,
  updateFoodCategory: PropTypes.func.isRequired,
  deleteFoodCategory: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors,
  foodCategories: state.foodCategories
});

export default connect(
  mapStateToProps,
  { getAllFoodCategory, addFoodCategory, updateFoodCategory, deleteFoodCategory }
)(TableList);
