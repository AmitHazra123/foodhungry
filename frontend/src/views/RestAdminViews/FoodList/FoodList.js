import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridItem from "components/RestAdminComponents/Grid/GridItem.js";
import GridContainer from "components/RestAdminComponents/Grid/GridContainer.js";
import Table from "components/RestAdminComponents/Table/Table.js";
import Card from "components/RestAdminComponents/Card/Card.js";
import CardHeader from "components/RestAdminComponents/Card/CardHeader.js";
import CardBody from "components/RestAdminComponents/Card/CardBody.js";
import Button from "components/RestAdminComponents/CustomButtons/Button.js";

import { getAllFood, deleteFood } from "../../../actions/foodAction";
import { getAllFoodCategory } from "../../../actions/foodCategoryAction";
import { Avatar, IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Create";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const useStyles = makeStyles(styles);

function TableList(props) {

  const [foods, setFoods] = useState([]);
  const [foodCategories, setFoodCategories] = useState([]);

  useEffect(() => {
    props.getAllFood();
    props.getAllFoodCategory();
  }, []);

  useEffect(() => {
    setFoods(props.foodReducer.foods);
    setFoodCategories(props.foodCategories);
  }, [props])

  function deleteFood(foodId) {
    props.deleteFood(foodId);
  }

  const classes = useStyles();

  let foodsTableData = [];
  
  if (foods.length > 0) {
    foodsTableData = foods.map(food => {
      let categoryNames = [];
      if(food.categoryIds.length > 0 && foodCategories.length > 0) {
        categoryNames = food.categoryIds.map(
          categoryId => {
            const filterFoodCategory = foodCategories.filter(
              foodCategory => foodCategory._id === categoryId
            )[0];
            if(filterFoodCategory) return filterFoodCategory.name;
          }
        );
      }
      return [
        (<Avatar src={food.image} alt={food.name} />),
        food.name,
        food.description,
        categoryNames.join(", "),
        food.price,
        food.deliveryTime,
        food.rating,
        (
          <IconButton onClick={() => window.location.href = `/manage-food/${food._id}`} aria-label="upload picture" component="span">
            <EditIcon />
          </IconButton>
        ),
        (
          <IconButton onClick={() => deleteFood(food._id)} color="secondary" aria-label="upload picture" component="span">
            <DeleteIcon />
          </IconButton>
        )
      ];
    });
  }

  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Food Store</h4>
            <p className={classes.cardCategoryWhite}>
              Here is the details of Foods
            </p>
            <Button color="primary" href="/manage-food/">Add Food</Button>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["Image", "Name", "Description", "Category", "Price", "Delivery Time", "Rating", "Edit", "Delete"]}
              tableData={foodsTableData}
            />
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  );
}

TableList.propTypes = {
  foodReducer: PropTypes.object.isRequired,
  foodCategories: PropTypes.object.isRequired,
  getAllFood: PropTypes.func.isRequired,
  getAllFoodCategory: PropTypes.func.isRequired,
  deleteFood: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  foodReducer: state.foodReducer,
  foodCategories: state.foodCategories
});

export default connect(
  mapStateToProps,
  { getAllFood, getAllFoodCategory, deleteFood }
)(TableList);
