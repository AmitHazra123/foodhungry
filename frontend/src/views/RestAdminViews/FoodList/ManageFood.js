import React, {useEffect, useState} from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import GridContainer from "components/RestAdminComponents/Grid/GridContainer";
import GridItem from "components/RestAdminComponents/Grid/GridItem";
import Card from "components/RestAdminComponents/Card/Card";
import CardHeader from "components/RestAdminComponents/Card/CardHeader";
import CardBody from "components/RestAdminComponents/Card/CardBody";
import CardAvatar from "components/RestAdminComponents/Card/CardAvatar";
import CustomInput from "components/RestAdminComponents/CustomInput/CustomInput";
import Button from "components/RestAdminComponents/CustomButtons/Button.js";

import Autocomplete from "@material-ui/lab/Autocomplete";
import { Checkbox, TextField, CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxIcon from '@material-ui/icons/CheckBox';

import { addFood, getFood, updateFood } from "../../../actions/foodAction";
import { getAllFoodCategory } from "../../../actions/foodCategoryAction";

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

const styles = {
    cardCategoryWhite: {
        "&,& a,& a:hover,& a:focus": {
            color: "rgba(255,255,255,.62)",
            margin: "0",
            fontSize: "14px",
            marginTop: "0",
            marginBottom: "0"
        },
        "& a,& a:hover,& a:focus": {
            color: "#FFFFFF"
        }
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none",
        "& small": {
            color: "#777",
            fontSize: "65%",
            fontWeight: "400",
            lineHeight: "1"
        }
    }
};

const useStyles = makeStyles(styles);


function ManageFood(props) {
    const [name, setName] = useState("");
    const [categories, setCategories] = useState([]);
    const [price, setPrice] = useState("");
    const [image, setImage] = useState("https://fertilitynetworkuk.org/wp-content/uploads/2017/01/Facebook-no-profile-picture-icon-620x389.jpg");
    const [files, setFiles] = useState([]);
    const [description, setDescription] = useState("");
    const [deliveryTime, setDeliveryTime] = useState("");

    const [foodCategories, setFoodCategories] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState({});

    const classes = useStyles();

    useEffect(() => {
        if(props.match.params[0] !== "") props.getFood(props.match.params[0]);
        props.getAllFoodCategory();
    }, []);

    useEffect(() => {
        if(props.foodReducer.food.name) setName(props.foodReducer.food.name);
        if(props.foodReducer.food.categories) setCategories(props.foodReducer.food.categories);
        if(props.foodReducer.food.price) setPrice(props.foodReducer.food.price);
        if(props.foodReducer.food.image) setImage(props.foodReducer.food.image);
        if(props.foodReducer.food.description) setDescription(props.foodReducer.food.description);
        if(props.foodReducer.food.deliveryTime) setDeliveryTime(props.foodReducer.food.deliveryTime);
        setFoodCategories(props.foodCategories);
        setError(props.errors.error);
        setIsLoading(false);
    }, [props]);

    function addFood(e) {
        e.preventDefault();
        setIsLoading(true);
        const formData = new FormData();
        formData.append("name", name);
        formData.append("categories", JSON.stringify(categories));
        formData.append("description", description);
        formData.append("price", price);
        formData.append("deliveryTime", deliveryTime);
        if(files.length > 0) formData.append("image", files[0]);
        props.addFood(formData);
    }

    function updateFood(e) {
        e.preventDefault();
        setIsLoading(true);
        const formData = new FormData();
        formData.append("name", name);
        formData.append("categories", JSON.stringify(categories));
        formData.append("description", description);
        formData.append("price", price);
        formData.append("deliveryTime", deliveryTime);
        if(files.length > 0)
            formData.append("image", files[0]);
        else
            formData.append("image", image);
        props.updateFood(props.match.params[0], formData);
    }

    const handleChangeImage = e => {
        if(e.target.files.length > 0) {
        const reader = new FileReader();
        reader.onload = function(e) {
            setImage(e.target.result);
        }
        reader.readAsDataURL(e.target.files[0]);
        setFiles(e.target.files);
        }
    };

    return (
        <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
            <Card plain>
            <CardHeader plain color="primary">
                <h4 className={classes.cardTitleWhite}>{(props.match.params[0] === "") ? "Add Food" : "Update Food"}</h4>
                <p className={classes.cardCategoryWhite}>{(props.match.params[0] === "") ? "Add" : "Update"} Food to your store</p>
            </CardHeader>
            <CardBody>
                <center>
                    <GridContainer md={6}>
                        <GridItem xs={12} sm={12} md={12}>
                            <br />
                            <br />
                            <input
                            accept="image/*"
                            id="text-button-file"
                            multiple
                            type="file"
                            onChange={handleChangeImage}
                            hidden
                            />
                            <label htmlFor="text-button-file">
                            <CardAvatar profile>
                                <img id="blah" src={image} alt="..." />
                            </CardAvatar>
                            <br />
                            <center>
                                {error.image ? (
                                <span style={{ color: "red" }}>{error.image}</span>
                                ) : (
                                ""
                                )}
                            </center>
                            </label>
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <CustomInput
                                labelText="Food Name"
                                id="food-name"
                                formControlProps={{
                                    fullWidth: true,
                                    onChange: e => setName(e.target.value)
                                }}
                                error={error.name}
                                value={name}
                            />
                            {error.name ? (
                            <span style={{ color: "red" }}>{error.name}</span>
                            ) : (
                            ""
                            )}
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <br />
                            <Autocomplete
                                multiple
                                id="food-categories"
                                options={foodCategories}
                                disableCloseOnSelect
                                getOptionLabel={category => category.name}
                                renderOption={(category, { selected }) => (
                                    <React.Fragment>
                                    <Checkbox
                                        icon={icon}
                                        checkedIcon={checkedIcon}
                                        style={{ marginRight: 8 }}
                                        checked={selected}
                                    />
                                    {category.name}
                                    </React.Fragment>
                                )}
                                renderInput={params => (
                                    <TextField
                                    {...params}
                                    fullWidth
                                    label="Food Categories"
                                    placeholder="Category"
                                    />
                                )}
                                onChange={(e, categories) => setCategories(categories)}
                            />
                            {error.categories ? (
                            <span style={{ color: "red" }}>{error.categories}</span>
                            ) : (
                            ""
                            )}
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <CustomInput
                            labelText="Price"
                            id="food-price"
                            formControlProps={{
                                fullWidth: true,
                                onChange: e => setPrice(e.target.value)
                            }}
                            error={error.price}
                            value={price}
                            />
                            {error.price ? (
                            <span style={{ color: "red" }}>{error.price}</span>
                            ) : (
                            ""
                            )}
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <CustomInput
                            labelText="Description"
                            id="food-description"
                            formControlProps={{
                                fullWidth: true,
                                onChange: e => setDescription(e.target.value)
                            }}
                            value={description}
                            error={error.description}
                            />
                            {error.description ? (
                            <span style={{ color: "red" }}>{error.description}</span>
                            ) : (
                            ""
                            )}
                        </GridItem>
                        <GridItem xs={12} sm={12} md={12}>
                            <CustomInput
                            labelText="Delivery Time"
                            id="delivery-time"
                            formControlProps={{
                                fullWidth: true,
                                onChange: e => setDeliveryTime(e.target.value)
                            }}
                            value={deliveryTime}
                            error={error.deliveryTime}
                            />
                            {error.deliveryTime ? (
                            <span style={{ color: "red" }}>{error.deliveryTime}</span>
                            ) : (
                            ""
                            )}
                        </GridItem>
                        <GridItem xs={12} sm={12} md={6}>
                            <br/>
                            {isLoading ? (<CircularProgress color="secondary" />) : ""}
                            {props.match.params.id ? "" : (
                                <Button color="primary" onClick={(props.match.params[0] === "") ? addFood : updateFood} disabled={isLoading}>
                                    {(props.match.params[0] === "") ? "Add" : "Update"} Food
                                </Button>
                            )}
                        </GridItem>
                        <GridItem xs={12} sm={12} md={6}>
                            <br />
                            <Button color="primary" onClick={() => window.location.href = "/admin/foods"}>
                                Cancel
                            </Button>
                        </GridItem>
                    </GridContainer>
                </center>
            </CardBody>
            </Card>
        </GridItem>
        </GridContainer>
    );
}

ManageFood.propTypes = {
    foodReducer: PropTypes.object.isRequired,
    foodCategories: PropTypes.object.isRequired,
    getFood: PropTypes.func.isRequired,
    getAllFoodCategory: PropTypes.func.isRequired,
    addFood: PropTypes.func.isRequired,
    updateFood: PropTypes.func.isRequired,
    errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    foodReducer: state.foodReducer,
    foodCategories: state.foodCategories,
    errors: state.errors
});

export default connect(
    mapStateToProps,
    { addFood, getFood, updateFood, getAllFoodCategory }
)(ManageFood);
