import React, {useState, useEffect} from 'react'
import {connect} from "react-redux";
import PropTypes from "prop-types";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridItem from "components/CourierComponents/Grid/GridItem";
import GridContainer from "components/CourierComponents/Grid/GridContainer";
import CustomInput from "components/CourierComponents/CustomInput/CustomInput.js";
import Button from "components/CourierComponents/CustomButtons/Button.js";
import Card from "components/CourierComponents/Card/Card.js";
import CardHeader from "components/CourierComponents/Card/CardHeader.js";
import CardAvatar from "components/CourierComponents/Card/CardAvatar.js";
import CardBody from "components/CourierComponents/Card/CardBody.js";
import CardFooter from "components/CourierComponents/Card/CardFooter.js";

import {
    getCurrentCourierProfile,
    updateCourierProfile,
    handleChangeCourierImage,
    deleteCourierProfile
  } from "actions/courierProfileAction";

const styles = {
    cardCategoryWhite: {
        color: "rgba(255,255,255,.62)",
        margin: "0",
        fontSize: "14px",
        marginTop: "0",
        marginBottom: "0"
    },
    cardTitleWhite: {
        color: "#FFFFFF",
        marginTop: "0px",
        minHeight: "auto",
        fontWeight: "300",
        fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
        marginBottom: "3px",
        textDecoration: "none"
    }
};

const useStyles = makeStyles(styles);

function ProfilePage(props) {

    const [email, setEmail] = useState("");
    const [name, setName] = useState("");
    const [bikeNo, setBikeNo] = useState("");
    const [mobile, setMobile] = useState("");
    const [address, setAddress] = useState("");
    const [image, setImage] = useState("https://fertilitynetworkuk.org/wp-content/uploads/2017/01/Facebook-no-profile-picture-icon-620x389.jpg");

    useEffect(() => {
        props.getCurrentCourierProfile();
    }, []);
    
    useEffect(() => {
        setEmail(props.auth.user.email);
        setName(props.auth.user.name);
        setBikeNo(props.auth.user.bikeNo);
        setMobile(props.auth.user.mobile);
        setAddress(props.auth.user.address);
        setImage(props.auth.user.image);
    }, [props])

    const classes = useStyles();

    const updateProfile = e => {
        const courierProfile = {
            name,
            email,
            bikeNo,
            mobile,
            address
        };
        props.updateCourierProfile(courierProfile);
    };
    
    const deleteProfile = e => {

    };    

    const handleChangeImage = e => {
        const fd = new FormData();
        fd.append("image", e.target.files[0], e.target.files[0].name);
        props.handleChangeCourierImage(fd);
    };

    return (
        <div>
            <GridContainer>
                <GridItem xs={12} sm={12} md={8}>
                    <Card>
                        <CardHeader color="primary">
                        <h4 className={classes.cardTitleWhite}>Profile</h4>
                        <p className={classes.cardCategoryWhite}>Complete your profile</p>
                        </CardHeader>
                        <CardBody>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={12}>
                                <CustomInput
                                    labelText="Email address"
                                    id="email-address"
                                    inputProps={{
                                    value: email !== undefined ? email : "",
                                    onChange: e => setEmail(e.target.value)
                                    }}
                                    formControlProps={{
                                    fullWidth: true
                                    }}
                                />
                                </GridItem>
                            </GridContainer>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={12}>
                                <CustomInput
                                    labelText="Name"
                                    id="first-name"
                                    inputProps={{
                                    value: name !== undefined ? name : "",
                                    onChange: e => setName(e.target.value)
                                    }}
                                    formControlProps={{
                                    fullWidth: true
                                    }}
                                />
                                </GridItem>
                            </GridContainer>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={12}>
                                <CustomInput
                                    labelText="Bike No."
                                    id="bike_no"
                                    inputProps={{
                                    type: "number",
                                    value: bikeNo !== undefined ? bikeNo : "",
                                    onChange: e => setBikeNo(e.target.value)
                                    }}
                                    formControlProps={{
                                    fullWidth: true
                                    }}
                                />
                                </GridItem>
                            </GridContainer>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={12}>
                                <CustomInput
                                    labelText="Mobile Number"
                                    id="mobile"
                                    inputProps={{
                                    type: "number",
                                    value: mobile !== undefined ? mobile : "",
                                    onChange: e => setMobile(e.target.value)
                                    }}
                                    formControlProps={{
                                    fullWidth: true
                                    }}
                                />
                                </GridItem>
                            </GridContainer>
                            <GridContainer>
                                <GridItem xs={12} sm={12} md={12}>
                                <CustomInput
                                    id="about-me"
                                    labelText="Address"
                                    inputProps={{
                                    value: address !== undefined ? address : "",
                                    onChange: e => setAddress(e.target.value)
                                    }}
                                    formControlProps={{
                                    fullWidth: true
                                    }}
                                />
                                </GridItem>
                            </GridContainer>
                        </CardBody>
                        <CardFooter>
                        <Button color="primary" onClick={updateProfile}>
                            Update Profile
                        </Button>
                        <Button color="primary" onClick={deleteProfile}>
                            Delete Profile
                        </Button>
                        </CardFooter>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                    <Card profile>
                        <input
                        accept="image/*"
                        id="text-button-file"
                        multiple
                        type="file"
                        onChange={handleChangeImage}
                        hidden
                        />
                        <label htmlFor="text-button-file">
                        <CardAvatar profile>
                            <img src={image} alt="..." height={400} width={400} />
                            <Button href="" component="span" color="white"></Button>
                        </CardAvatar>
                        </label>
                        <CardBody profile>
                        <h6 className={classes.cardCategory}>
                            {email}
                        </h6>
                        <h4 className={classes.cardTitle}>
                            {name}
                        </h4>
                        <p className={classes.description}>
                            Address
                            <br />
                            {address}
                        </p>
                        </CardBody>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    )
}

ProfilePage.propTypes = {
    auth: PropTypes.object.isRequired,
    getCurrentCourierProfile: PropTypes.func.isRequired,
    updateCourierProfile: PropTypes.func.isRequired,
    handleChangeCourierImage: PropTypes.func.isRequired,
    deleteCourierProfile: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(
    mapStateToProps, {
        getCurrentCourierProfile,
        updateCourierProfile,
        handleChangeCourierImage,
        deleteCourierProfile
    }
)(ProfilePage);