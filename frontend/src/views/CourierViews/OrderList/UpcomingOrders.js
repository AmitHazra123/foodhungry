/*eslint-disable*/
import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Hidden from "@material-ui/core/Hidden";
// core components
import GridItem from "components/RestAdminComponents/Grid/GridItem.js";
import GridContainer from "components/RestAdminComponents/Grid/GridContainer.js";
import Card from "components/RestAdminComponents/Card/Card.js";
import CardHeader from "components/RestAdminComponents/Card/CardHeader.js";
import CardBody from "components/RestAdminComponents/Card/CardBody.js";

import styles from "assets/jss/restadmin-kit/views/iconsStyle.js";
import Orders from "./sections/Orders";

const useStyles = makeStyles(styles);

export default function UpcomingOrders() {
  const classes = useStyles();
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card plain>
          <CardHeader plain color="primary">
            <h4 className={classes.cardTitleWhite}>Upcoming Orders</h4>
            <p className={classes.cardCategoryWhite}>
              Your Upcoming Orders
            </p>
          </CardHeader>
          <CardBody> 
            <Orders/>
            <Orders/>
          </CardBody>
        </Card>
      </GridItem>
    </GridContainer>
  );
}
