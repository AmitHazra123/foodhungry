import React, { useEffect, useState } from "react";
import classNames from "classnames";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { Grid, Typography } from "@material-ui/core";

// core components
import Header from "components/UserComponents/Header/Header.js";
import Footer from "components/UserComponents/Footer/Footer.js";
import Parallax from "components/UserComponents/Parallax/Parallax.js";
import HeaderLinks from "components/UserComponents/Header/HeaderLinks.js";

// views components
import SectionNavbars from "views/UserViews/Components/Sections/SectionNavbars";

// styles
import styles from "assets/jss/user-kit/views/restaurants";

// load actions
import { getRestaurant } from "actions/restaurantViewAction";

const useStyles = makeStyles(styles);

// const theme = createMuiTheme();

function Restaurant(props) {
  const [name, setName] = useState("Imperio Restaurant");
  const [image, setImage] = useState(
    "https://media.gettyimages.com/photos/authentic-indian-food-picture-id639389404?s=612x612"
  );
  const [address, setAddress] = useState(
    "VARTHUR KODI, PALM MEADOWS, RAMAGONDANAHALLI, WHITEFIELD, Bengaluru"
  );
  const [description, setDescription] = useState("");
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    const restaurantId = props.match.params.id;
    props.getRestaurant(restaurantId);
  }, []);

  useEffect(() => {
    const { restaurant } = props.restaurantReducer;
    if (restaurant !== undefined) {
      setName(restaurant.name);
      setImage(restaurant.image);
      setAddress(restaurant.address);
      setDescription(restaurant.description);
      setCategories(restaurant.categories);
    }
  }, [props]);

  const classes = useStyles();
  const { ...rest } = props;
  return (
    <div>
      <Header
        brand="FoodHungry"
        rightLinks={<HeaderLinks />}
        fixed
        color="transparent"
        changeColorOnScroll={{
          height: 300,
          color: "white",
        }}
        {...rest}
      />
      <Parallax>
        <div>
          <Grid container lg={12} md={12} style={{ backgroundColor: "white" }}>
            <Grid item lg={6} md={6}>
              <div
                style={{
                  height: 700,
                }}
              >
                <img
                  src={image}
                  style={{
                    height: 700,
                  }}
                  alt="Restaurant Details"
                />
              </div>
            </Grid>
            <Grid item lg={6} md={6}>
              <div
                style={{
                  height: 700,
                  paddingLeft: 100,
                  paddingTop: 150,
                  backgroundColor: "#EE5533",
                }}
              >
                <Grid container lg={12} md={12}>
                  <Grid item lg={12} md={12}>
                    <h1 className={classes.title}>{name}</h1>
                  </Grid>
                  <Grid item lg={12} md={12}>
                    <Typography variant="h4">
                      New Udupi Upahar - Whitefield
                    </Typography>
                  </Grid>
                  <Grid item lg={12} md={12} style={{ marginTop: 10 }}>
                    <Typography variant="subtitle1">
                      1. South Indian, 2. Breakfast, 3. Lunch
                    </Typography>
                  </Grid>
                  <Grid container item lg={12} md={12}>
                    <Grid item lg={6} md={6}>
                      <Typography>Rating</Typography>
                    </Grid>
                    <Grid item lg={6} md={6}>
                      <Typography>Delivery Time</Typography>
                    </Grid>
                  </Grid>
                  <Grid container item lg={12} md={12}>
                    <Grid item lg={6} md={6}>
                      <Typography>4.3</Typography>
                    </Grid>
                    <Grid item lg={6} md={6}>
                      <Typography>30 min</Typography>
                    </Grid>
                  </Grid>
                  <Grid item lg={12} md={6} style={{ marginTop: 10 }}>
                    <Typography variant="subtitle1">{address}</Typography>
                  </Grid>
                </Grid>
              </div>
            </Grid>
          </Grid>
        </div>
      </Parallax>

      <div className={classNames(classes.main, classes.mainRaised)}>
        <SectionNavbars categories={categories} />
      </div>
      <Footer />
    </div>
  );
}

Restaurant.propTypes = {
  restaurantReducer: PropTypes.object.isRequired,
  getRestaurant: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  restaurantReducer: state.restaurantReducer,
});

export default connect(
  mapStateToProps,
  { getRestaurant }
)(Restaurant);
