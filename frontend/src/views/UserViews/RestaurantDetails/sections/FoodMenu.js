import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import SwipeableViews from "react-swipeable-views";
import { makeStyles, createMuiTheme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Zoom from "@material-ui/core/Zoom";
import Fab from "@material-ui/core/Fab";
import Cart from "@material-ui/icons/AddShoppingCartOutlined";
import styles from "assets/jss/user-kit/components/foodmenuStyle";
import { primaryColor } from "assets/jss/user-kit";
import { ThemeProvider } from "@material-ui/styles";

import FoodCard from "components/UserComponents/Card/FoodCard";
import { Grid, Badge } from "@material-ui/core";
// import CloseIcon from '@material-ui/icons/Close';

import FoodCart from "components/UserComponents/FoodCart/FoodCart";
import { openCart } from "actions/cartActions";
import { getAllFoodByCategories } from "actions/foodAction";

import { connect } from "react-redux";

function TabPanel(props) {
  const [foods, setFoods] = useState([]);

  useEffect(() => {
    setFoods(props.foods);
  }, [props]);

  return (
    <div style={{ overflow: "hidden" }}>
      <Grid
        container
        item
        spacing={4}
        lg={12}
        style={{ marginTop: 30, marginBottom: 30 }}
      >
        {foods.length > 0 ? (
          foods.map((food) => (
            <Grid item lg={4}>
              <FoodCard food={food} />
            </Grid>
          ))
        ) : (
          <div></div>
        )}
      </Grid>
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `action-tab-${index}`,
    "aria-controls": `action-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(styles);

function FoodMenu(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [categories, setCategories] = useState([]);
  const [foods, setFoods] = useState([]);

  useEffect(() => {
    if (props.categories !== undefined) {
      if (props.categories.length > 0) {
        const categoryId = props.categories[0]._id;
        const restaurantId = props.match.params.id;
        props.getAllFoodByCategories(restaurantId, categoryId);
      }
      setCategories(props.categories);
    }
  }, props.categories);

  useEffect(() => {
    setFoods(props.foodReducer.foods); 
  }, [props.foodReducer]);

  function handleClickOpen(props) {
    const cartData = {
      open: true
    };
    props.openCart(cartData);
  }

  function handleChange(event, newValue) {
    const restaurantId = props.match.params.id;
    const categoryId = categories[newValue]._id;
    props.getAllFoodByCategories(restaurantId, categoryId);
    setValue(newValue);
  }

  function handleChangeIndex(index) {
    setValue(index);
  }

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: primaryColor,
      },
      secondary: {
        main: "#f44336",
      },
    },
  });

  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen,
  };

  const fab = {
    color: "primary",
    className: classes.fab,
    icon: <Cart />,
    label: "Add To Cart",
  };

  return (
    <ThemeProvider theme={theme}>
      <FoodCart />
      <div className={classes.root}>
        <AppBar
          position="static"
          color="default"
          style={{ width: "auto", marginLeft: 60, marginRight: 60 }}
        >
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            aria-label="action tabs example"
          >
            {categories.map((category, index) => {
              return <Tab label={category.name} {...a11yProps(index)} />;
            })}
          </Tabs>
        </AppBar>
        <SwipeableViews
          axis={theme.direction === "rtl" ? "x-reverse" : "x"}
          index={value}
          onChangeIndex={handleChangeIndex}
        >
          {categories.map((category) => {
            return (
              <TabPanel
                index={value}
                dir={theme.direction}
                foods={foods}
              ></TabPanel>
            );
          })}
        </SwipeableViews>
        <Zoom
          key={fab.color}
          in={true}
          title="View Cart"
          timeout={transitionDuration}
          style={{
            transitionDelay: `${transitionDuration.exit}ms`,
          }}
          unmountOnExit
        >
          <Fab
            aria-label={fab.label}
            className={fab.className}
            onClick={() => handleClickOpen(props)}
          >
            <Badge badgeContent={props.cart.foods.length} color="primary">
              {fab.icon}
            </Badge>
          </Fab>
        </Zoom>
      </div>
    </ThemeProvider>
  );
}

FoodMenu.propTypes = {
  foodReducer: PropTypes.object.isRequired,
  openCart : PropTypes.func.isRequired,
  getAllFoodByCategories: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  cart: state.cart,
  foodReducer: state.foodReducer,
});

export default connect(
  mapStateToProps,
  { openCart, getAllFoodByCategories }
)(withRouter(FoodMenu));
