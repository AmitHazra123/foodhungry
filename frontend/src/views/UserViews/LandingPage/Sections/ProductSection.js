import React, {useEffect, useState} from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// import Button from "components/CustomButtons/Button.js";
import Card from "components/UserComponents/Card/Card";

import styles from "assets/jss/user-kit/views/componentsSections/basicsStyle.js";
import { Grid } from "@material-ui/core";

// load actions
import {restaurantsWithNearestLocation} from "../../../../actions/restaurantViewAction";

const useStyles = makeStyles(styles);

function ProductSection(props) {

  const [restaurants, setRestaurants] = useState([]);

  useEffect(() => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        const location = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        };
        props.restaurantsWithNearestLocation(location);
      });
    }
  }, []);

  useEffect(() => {
    setRestaurants(props.restaurantReducer.restaurants);
  }, [props]);

  const classes = useStyles();

  return (
    <div className={classes.sections}>
      <div className={classes.container}>
        <div className={classes.title}>
          <h2>These Are Some Beautiful Restaurants as per Your Location</h2>
        </div>
        <div id="buttons">
          <Grid container direction="row" lg={12} spacing={2}>
            {
              restaurants.length > 0 ?(
                restaurants.map(restaurant =>(
                  <Grid item lg={4}>
                    <Card restaurant={restaurant} />
                  </Grid>
                  ))
              ):(
                <div></div>
              )
            }
          </Grid>
        </div>
      </div>
    </div>
  );
}

ProductSection.propTypes = {
  restaurantReducer: PropTypes.object.isRequired,
  restaurantsWithNearestLocation: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  restaurantReducer: state.restaurantReducer
});

export default connect(
  mapStateToProps,
  {restaurantsWithNearestLocation}
)(ProductSection);