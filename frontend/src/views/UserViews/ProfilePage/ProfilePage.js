import React, {useState, useEffect} from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import {connect} from 'react-redux';

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import Header from "components/UserComponents/Header/Header.js";
import Footer from "components/UserComponents/Footer/Footer.js";
import Button from "components/UserComponents/CustomButtons/Button.js";
import GridContainer from "components/UserComponents/Grid/GridContainer.js";
import GridItem from "components/UserComponents/Grid/GridItem.js";
import HeaderLinks from "components/UserComponents/Header/HeaderLinks.js";
import Parallax from "components/UserComponents/Parallax/Parallax.js";

// styles
import styles from "assets/jss/user-kit/views/profilePage.js";
import Card from "components/RestAdminComponents/Card/Card.js";
import CardHeader from "components/RestAdminComponents/Card/CardHeader.js";
import CardBody from "components/RestAdminComponents/Card/CardBody.js";
import CardFooter from "components/RestAdminComponents/Card/CardFooter.js";
import CustomInput from "components/RestAdminComponents/CustomInput/CustomInput";

// action
import {
  getCurrentUserProfile,
  handleChangeUserImage,
  updateUserProfile,
  deleteUserProfile
} from "actions/userProfileAction";

const useStyles = makeStyles(styles);

function ProfilePage(props) {
  const [image, setimage] = useState("https://res.cloudinary.com/amit-hazra/image/upload/v1572948963/xdb4atodqpdlp5js00gq.jpg")
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [mobile, setMobile] = useState("");
  const [address, setAddress] = useState("");

  const classes = useStyles();
  const { ...rest } = props;
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );

  useEffect(() => {
    props.getCurrentUserProfile();
  }, [])

  useEffect(() => {
    setimage(props.auth.user.image);
    setName(props.auth.user.name);
    setEmail(props.auth.user.email);
    setMobile(props.auth.user.mobile);
    setAddress(props.auth.user.address);
  }, [props]);

  const updateProfile = e => {
    const user = {
      email,
      name,
      mobile,
      address
    };

    props.updateUserProfile(user);
  }

  const deleteProfile = e => {
    // alert("Delete Profile");
  }

  const handleChangeImage = e => {
    const fd = new FormData();
    fd.append("image", e.target.files[0], e.target.files[0].name);
    props.handleChangeUserImage(fd);
  };

  return (
    <div>
      <Header
        brand="FoodHungry"
        rightLinks={<HeaderLinks />}
        fixed
        color="transparent"
        changeColorOnScroll={{
          height: 300,
          color: "white",
        }}
        {...rest}
      />
      <Parallax small filter image={require("assets/img/profile-bg.jpg")} />
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div>
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={12} sm={12} md={6}>
                <div className={classes.profile}>
                  <div>
                    <input
                      accept="image/*"
                      id="text-button-file"
                      multiple
                      type="file"
                      onChange={handleChangeImage}
                      hidden
                    />
                    <label htmlFor="text-button-file">
                      <img style={{height: 150, width: 150}} src={image} alt="..." className={imageClasses} />
                    </label>
                  </div>
                </div>
              </GridItem>
            </GridContainer>
            <GridContainer justify="center">
              <GridItem xs={12} sm={12} md={12} lg={12} className={classes.navWrapper}>
              <Card>
                <CardHeader color="primary">
                  <h3 classNames={classes.title, classes.cartTitleWhite}>{name}</h3>
                  <p className={classes.cardCategoryWhite}>Complete your profile</p>
                </CardHeader>
                <CardBody>
                  <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                      <CustomInput
                        labelText="Email address"
                        id="email-address"
                        inputProps={{
                          value: email,
                          onChange: e => setEmail(e.target.value)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                    </GridItem>
                  </GridContainer>
                  <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                      <CustomInput
                        labelText="User Name"
                        id="first-name"
                        inputProps={{
                          value: name,
                          onChange: e => setName(e.target.value)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                    </GridItem>
                  </GridContainer>
                  <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                      <CustomInput
                        labelText="Mobile Number"
                        id="mobile"
                        inputProps={{
                          type: "number",
                          value: mobile,
                          onChange: e => setMobile(e.target.value)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                    </GridItem>
                  </GridContainer>
                  <GridContainer>
                    <GridItem xs={12} sm={12} md={12}>
                      <CustomInput
                        id="about-me"
                        labelText="Address"
                        inputProps={{
                          value: address,
                          onChange: e => setAddress(e.target.value)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                    </GridItem>
                  </GridContainer>
                </CardBody>
                <CardFooter>
                  <Button color="primary" onClick={updateProfile}>
                    Update Profile
                  </Button>
                  <Button color="primary" onClick={deleteProfile}>
                    Delete Profile
                  </Button>
                </CardFooter>
              </Card>
              </GridItem>
            </GridContainer>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

ProfilePage.propTypes = {
  auth: PropTypes.object.isRequired,
  getCurrentUserProfile: PropTypes.func.isRequired,
  handleChangeUserImage: PropTypes.func.isRequired,
  updateUserProfile: PropTypes.func.isRequired,
  deleteUserProfile: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps, {
    getCurrentUserProfile,
    handleChangeUserImage,
    updateUserProfile,
    deleteUserProfile
  }
)(ProfilePage);