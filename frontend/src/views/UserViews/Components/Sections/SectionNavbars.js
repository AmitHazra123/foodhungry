import React from "react";
import { makeStyles } from "@material-ui/core/styles";
// core components
import GridContainer from "components/UserComponents/Grid/GridContainer.js";
import GridItem from "components/UserComponents/Grid/GridItem.js";

// styles
import styles from "assets/jss/user-kit/views/componentsSections/navbarsStyle.js";

// views
import FoodMenu from "views/UserViews/RestaurantDetails/sections/FoodMenu";

const useStyles = makeStyles(styles);

export default function SectionNavbars(props) {
  const classes = useStyles();
  let categories = [];
  categories = props.categories;
  return (
    <div className={classes.section}>
      <div className={classes.container}>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <div className={classes.title}>
              <h3>Food Menu</h3>
            </div>
            <FoodMenu categories={categories} />
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
