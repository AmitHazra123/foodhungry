import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LocalDining from "@material-ui/icons/LocalDining";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
// core components/views for Admin layout
import DashboardPage from "views/CourierViews/Dashboard/Dashboard";
import UpcomingOrders from "views/CourierViews/OrderList/UpcomingOrders";
import PastOrders from "views/CourierViews/OrderList/PastOrders";
import PendingPayments from "views/CourierViews/OrderList/PendingPayments";
import Incentives from "views/CourierViews/Incentives/Incentives";
import Profile from "views/CourierViews/Profile/ProfilePage";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/courier"
  },
  {
    path: "/upcomings",
    name: "Upcoming Orders",
    rtlName: "الرموز",
    icon: LocalDining,
    component: UpcomingOrders,
    layout: "/courier"
  },
  {
    path: "/pasts",
    name: "Past Orders",
    rtlName: "الرموز",
    icon: "restaurants",
    component: PastOrders,
    layout: "/courier"
  },
  {
    path: "/pendingpayment",
    name: "Pending Payments",
    rtlName: "الرموز",
    icon: LocalDining,
    component: PendingPayments,
    layout: "/courier"
  },
  {
    path: "/incentives",
    name: "Incentives",
    rtlName: "الرموز",
    icon: LocalDining,
    component: Incentives,
    layout: "/courier"
  },
  {
    path: "/profile",
    name: "Courier Profile",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Person,
    component: Profile,
    layout: "/courier"
  }
];

export default dashboardRoutes;
