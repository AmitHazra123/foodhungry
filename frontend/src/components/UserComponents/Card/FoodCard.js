import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
// import CardContent from '@material-ui/core/CardContent';
import CardActions from "@material-ui/core/CardActions";
// import Avatar from '@material-ui/core/Avatar';
import Typography from "@material-ui/core/Typography";
import { CardActionArea, Paper, createMuiTheme } from "@material-ui/core";
import styles from "assets/jss/user-kit/components/cardStyle";
import { withRouter } from "react-router-dom";
// import CloseIcon from '@material-ui/icons/Close';
import { ThemeProvider } from "@material-ui/styles";
import { primaryColor } from "assets/jss/user-kit.js";
import FoodCart from "components/UserComponents/FoodCart/FoodCart";
import { addFoods } from "actions/cartActions";

import { connect } from "react-redux";
import PropTypes from "prop-types";

const useStyles = makeStyles(styles);

const theme = createMuiTheme({
  palette: {
    primary: {
      main: primaryColor,
    },
    secondary: {
      main: "#f44336",
    },
  },
});

function handleClickOpen(props) {
  if(props.food !== undefined) {
    const {food} = props;
    const cartData = {
      open: true,
      food: {
        foodId: food._id,
        name: food.name,
        quantity: 1,
        price: food.price
      }
    };
    props.addFoods(cartData);
  }
}

function FoodCard(props) {
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [image, setImage] = useState("");
  const [deliveryTime, setDeliveryTime] = useState("");
  const [rating, setRating] = useState(0);
  const [price, setPrice] = useState(0);

  useEffect(() => {
    if (props.food !== undefined) {
      setName(props.food.name);
      setDescription(props.food.description);
      setImage(props.food.image);
      setDeliveryTime(props.food.deliveryTime);
      setRating(props.food.rating);
      setPrice(props.food.price);
    }
  }, [props]);

  const classes = useStyles();

  return (
    <ThemeProvider theme={theme}>
      <FoodCart />
      <div>
        <Card className={classes.card}>
          <CardActionArea onClick={() => handleClickOpen(props)}>
            <CardHeader title={name} subheader={description} />
            <CardMedia
              className={classes.media}
              image={image}
              title="Add To Cart"
            />
          </CardActionArea>
          <CardActions>
            <Paper style={{ boxShadow: "none", backgroundColor: "#F6F6F6" }}>
              <Typography variant="body2" color="textSecondary" component="p">
                {deliveryTime}
              </Typography>
            </Paper>
            <Paper style={{ boxShadow: "none", backgroundColor: "#F6F6F6" }}>
              <Typography variant="body2" color="textSecondary" component="p">
                {rating} <i class="fas fa-star"></i> (500+ )
              </Typography>
            </Paper>
            <Paper style={{ boxShadow: "none", backgroundColor: "#F6F6F6" }}>
              <Typography variant="body2" color="textSecondary" component="p">
                Rs. {price}
              </Typography>
            </Paper>
          </CardActions>
        </Card>
      </div>
    </ThemeProvider>
  );
}

FoodCard.propTypes = {
  addFoods: PropTypes.func.isRequired,
};

export default connect(
  null,
  { addFoods }
)(withRouter(FoodCard));
