import React, {useState, useEffect} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { CardActionArea, Paper } from '@material-ui/core';
import styles from 'assets/jss/user-kit/components/cardStyle';
import { withRouter } from 'react-router-dom';

const useStyles = makeStyles(styles);

const _hankdleClickRest = id => {
  window.location.href = `/restaurant/${id}`;
};

function RecipeReviewCard(props) {

  const [name, setName] = useState("");
  const [image, setImage] = useState("");
  const [address, setAddress] = useState("");
  const [description, setDescription] = useState("");
  const [deliveryTime, setDeliveryTime] = useState("");
  const [rating, setRating] = useState(0);

  useEffect(() => {
    if(props.restaurant !== undefined) {
      setName(props.restaurant.name);
      setImage(props.restaurant.image);
      setAddress(props.restaurant.address);
      setDescription(props.restaurant.description);
      setDeliveryTime(props.restaurant.deliveryTime);
      setRating(props.restaurant.rating);
    }
  }, [props])

  const classes = useStyles();
  let id = "";
  if(props.restaurant !== undefined)  id = props.restaurant._id;

  return (
    <Card className={classes.card}>
      <CardActionArea onClick={() => _hankdleClickRest(id)}>
        <CardHeader
          avatar={
            <Avatar aria-label='recipe' className={classes.avatar}>
              {name.charAt(0)}
            </Avatar>
          }
          title={name}
          subheader={address}
        />
        <CardMedia
          className={classes.media}
          image={image}
          title='Paella dish'
        />
        <CardContent>
          <Typography variant='body2' color='textSecondary' component='p'>
            {description}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Paper style={{ boxShadow: 'none', backgroundColor: '#F6F6F6' }}>
          <Typography variant='body2' color='textSecondary' component='p'>
            {deliveryTime}
          </Typography>
        </Paper>
        <Paper style={{ boxShadow: 'none', backgroundColor: '#F6F6F6' }}>
          <Typography variant='body2' color='textSecondary' component='p'>
            {rating} <i class='fas fa-star'></i> (500+ )
          </Typography>
        </Paper>
      </CardActions>
    </Card>
  );
}

export default withRouter(RecipeReviewCard);
