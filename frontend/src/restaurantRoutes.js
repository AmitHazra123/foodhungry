import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LocalDining from "@material-ui/icons/LocalDining";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
// core components/views for Admin layout
import DashboardPage from "views/RestAdminViews/Dashboard/Dashboard.js";
import RestaurantProfile from "views/RestAdminViews/RestaurantProfile/RestaurantProfile.js";
import FoodList from "views/RestAdminViews/FoodList/FoodList.js";
import FoodCategories from "views/RestAdminViews/FoodCategories/FoodCategories.js";
import Maps from "views/RestAdminViews/Maps/Maps.js";
import NotificationsPage from "views/RestAdminViews/Notifications/Notifications.js";
import UpcomingOrder from "views/RestAdminViews/OrderList/UpcomingOrder";
import PastOrder from "views/RestAdminViews/OrderList/PastOrder";
import CancelledOrder from "views/RestAdminViews/OrderList/CancelledOrder";
import PendingPayment from "views/RestAdminViews/OrderList/PendingPayment";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/upcomings",
    name: "Upcoming Orders",
    rtlName: "الرموز",
    icon: LocalDining,
    component: UpcomingOrder,
    layout: "/admin"
  },
  {
    path: "/cancelled",
    name: "Cancelled Orders",
    rtlName: "الرموز",
    icon: LocalDining,
    component: CancelledOrder,
    layout: "/admin"
  },
  {
    path: "/pendingpayment",
    name: "Pending Payments",
    rtlName: "الرموز",
    icon: LocalDining,
    component: PendingPayment,
    layout: "/admin"
  },
  {
    path: "/pasts",
    name: "Past Orders",
    rtlName: "الرموز",
    icon: "restaurants",
    component: PastOrder,
    layout: "/admin"
  },

  {
    path: "/foods",
    name: "Foods",
    rtlName: "قائمة الجدول",
    icon: "cake",
    component: FoodList,
    layout: "/admin"
  },
  {
    path: "/foodcategories",
    name: "Food Categories",
    rtlName: "قائمة الجدول",
    icon: "cake",
    component: FoodCategories,
    layout: "/admin"
  },
  {
    path: "/maps",
    name: "Maps",
    rtlName: "خرائط",
    icon: LocationOn,
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/notifications",
    name: "Notifications",
    rtlName: "إخطارات",
    icon: Notifications,
    component: NotificationsPage,
    layout: "/admin"
  },
  {
    path: "/profile",
    name: "Restaurant Profile",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Person,
    component: RestaurantProfile,
    layout: "/admin"
  }
];

export default dashboardRoutes;
