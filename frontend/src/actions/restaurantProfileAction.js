import {
    // DELETE_RESTAURANT_PROFILE,
    SET_ERRORS,
    SET_CURRENT_USER
} from "./types";
import axios from "axios";
import { api } from "utils";

const getCurrentRestaurantProfileSuccess = profile => {
    return {
        type: SET_CURRENT_USER,
        payload: profile
    };
}

// const deleteRestaurantProfileSuccess = () => {
//     return {
//         type: DELETE_RESTAURANT_PROFILE,
//         payload: {}
//     };
// }

const setError = error => {
    return {
        type: SET_ERRORS,
        payload: error
    }
}

export const getCurrentRestaurantProfile = () => dispatch => {
    axios
      .get(api.BASE_URL + api.GET_CURRENT_RESTAURANT_ADMIN)
      .then(res => dispatch(getCurrentRestaurantProfileSuccess(res.data)))
      .catch(error => console.log(error));
}

export const handleChangeRestaurantImage = adminProfile => dispatch => {
    axios
      .put(api.BASE_URL + api.UPDATE_RESTAURANT_PROFILE_PICTURE, adminProfile, {
        headers: {
          accept: "application/json",
          "Accept-Language": "en-US,en;q=0.8",
          "Content-Type": `multipart/form-data; boundary=${adminProfile._boundary}`
        }
      })
      .then(res => dispatch(getCurrentRestaurantProfile()))
      .catch(error => dispatch(setError(error.response.data)));
}

export const updateRestaurantProfile = adminProfile =>  dispatch => {
    axios
      .put(api.BASE_URL + api.UPDATE_RESTAURANT_PROFILE, adminProfile)
      .then(res => dispatch(getCurrentRestaurantProfile(res.data)))
      .catch(error => dispatch(setError(error.response.data)));
}

export const deleteRestaurantProfile = () => dispatch => {
    // ignore it
}