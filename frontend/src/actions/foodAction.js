import axios from "axios";

import { api } from "../utils.js";
import { GET_ALL_FOOD, GET_FOOD, ADD_FOOD, DELETE_FOOD, SET_ERRORS, UPDATE_FOOD } from "./types";

// actions success method
const getAllFoodSuccess = (foods) => {
  return {
    type: GET_ALL_FOOD,
    payload: foods,
  };
};

const getFoodSuccess = food => {
  return {
    type: GET_FOOD,
    payload: food
  };
};

const addFoodSuccess = (food) => {
  return {
    type: ADD_FOOD,
    payload: food,
  };
};

const updateFoodSuccess = (food) => {
  return {
    type: UPDATE_FOOD,
    payload: food
  }
}

const deleteFoodSuccess = (foodId) => {
  return {
    type: DELETE_FOOD,
    payload: foodId,
  };
};

const setErrorSuccess = (errors) => {
  return {
    type: SET_ERRORS,
    payload: errors,
  };
};

// actions
// to get all food
export const getAllFood = () => (dispatch) => {
  axios
    .get(api.BASE_URL + api.GET_ALL_FOOD)
    .then((res) => dispatch(getAllFoodSuccess(res.data)))
    .catch((error) => dispatch(setErrorSuccess(error.response.data)));
};

// to get food
export const getFood = foodId => dispatch => {
  axios
    .get(api.BASE_URL + api.GET_FOOD + foodId)
    .then(res => dispatch(getFoodSuccess(res.data)))
    .catch(error => dispatch(setErrorSuccess(error.response.data)));
}

// to add a food
export const addFood = (food) => (dispatch) => {
  axios
    .post(api.BASE_URL + api.ADD_FOOD, food)
    .then((res) => {
      dispatch(addFoodSuccess(res.data));
      window.location.href = "/admin/foods";
    })
    .catch((error) => dispatch(setErrorSuccess(error.response.data)));
};

// to update food
export const updateFood = (foodId, food) => dispatch => {
  axios
    .put(api.BASE_URL + api.UPDATE_FOOD + foodId, food)
    .then(res => {
      dispatch(updateFoodSuccess(res.data));
      window.location.href = "/admin/foods";
    })
    .catch(error => dispatch(setErrorSuccess(error.response.data)))
}

// to delete a food
export const deleteFood = (foodId) => (dispatch) => {
  axios
    .delete(api.BASE_URL + api.DELETE_FOOD + foodId)
    .then((res) => dispatch(deleteFoodSuccess(foodId)))
    .catch((error) => dispatch(setErrorSuccess(error.response.data)));
};

// user part
export const getAllFoodByCategories = (restaurantId, categoryId) => (
  dispatch
) => {
  axios
    .get(
      api.BASE_URL +
        api.GET_ALL_FOOD_BY_CATEGORIES +
        "?restaurantId=" +
        restaurantId +
        "&categoryId=" +
        categoryId
    )
    .then((res) => dispatch(getAllFoodSuccess(res.data)))
    .catch((error) => setErrorSuccess(error.response.data));
};
