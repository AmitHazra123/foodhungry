import axios from "axios";
import {SEARCH_RESTAURANTS, VIEW_RESTAURANTS_WITH_LOCATION, SET_ERRORS, GET_RESTAURANT} from "./types";
import {api} from "../utils";

// types and payload
const searchRestaurantsSuccess = restaurants => {
    return {
        type: SEARCH_RESTAURANTS,
        payload: restaurants
    }
}

const restaurantsWithNearestLocationSuccess = restaurants => {
    return {
        type: VIEW_RESTAURANTS_WITH_LOCATION,
        payload: restaurants
    }
}

const getRestaurantSuccess = restaurant => {
    return {
        type: GET_RESTAURANT,
        payload: restaurant
    }
}

const setErrorSuccess = errors => {
    return {
      type: SET_ERRORS,
      payload: errors
    };
};

// view restaurants based on foods or restaurant name
export const searchRestaurants = () => dispatch => {
    // let we work here later
}

// view restaurants based on location
export const restaurantsWithNearestLocation = location => dispatch => {
    axios.post(api.BASE_URL + api.GET_ALL_RESTAURANT_BY_LOCATION, location).then(res => {
        dispatch(restaurantsWithNearestLocationSuccess(res.data));
    }).catch(error => {
        dispatch(setErrorSuccess(error.response.data));
    });
}
// view a particular restaurant
export const getRestaurant = restaurantId => dispatch => {
    axios.get(api.BASE_URL + api.GET_RESTAURANT + restaurantId).then(res => {
        return dispatch(getRestaurantSuccess(res.data));
    }).catch(error => {
        return dispatch(setErrorSuccess(error.response.data));
    });
}