import {
    // DELETE_USER_PROFILE,
    SET_ERRORS,
    SET_CURRENT_USER
} from "./types";
import axios from "axios";
import { api } from "utils";

const getCurrentUserProfileSuccess = profile => {
    return {
        type: SET_CURRENT_USER,
        payload: profile
    };
}

// const deleteUserProfileSuccess = () => {
//     return {
//         type: DELETE_USER_PROFILE,
//         payload: {}
//     };
// }

const setError = error => {
    return {
        type: SET_ERRORS,
        payload: error
    }
}

export const getCurrentUserProfile = () => dispatch => {
    axios
      .get(api.BASE_URL + api.GET_CURRENT_USER)
      .then(res => dispatch(getCurrentUserProfileSuccess(res.data)))
      .catch(error => console.log(error));
}

export const handleChangeUserImage = adminProfile => dispatch => {
    axios
      .put(api.BASE_URL + api.UPDATE_USER_PROFILE_PICTURE, adminProfile, {
        headers: {
          accept: "application/json",
          "Accept-Language": "en-US,en;q=0.8",
          "Content-Type": `multipart/form-data; boundary=${adminProfile._boundary}`
        }
      })
      .then(res => dispatch(getCurrentUserProfile()))
      .catch(error => dispatch(setError(error.response.data)));
}

export const updateUserProfile = adminProfile =>  dispatch => {
    axios
      .put(api.BASE_URL + api.UPDATE_USER_PROFILE, adminProfile)
      .then(res => dispatch(getCurrentUserProfile(res.data)))
      .catch(error => dispatch(setError(error.response.data)));
}

export const deleteUserProfile = () => dispatch => {
    // ignore it
}