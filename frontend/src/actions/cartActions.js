import { ADD_FOODS, SAVE_CART, OPEN_CART } from './types';

export const addFoods = cartData => dispatch => {
  dispatch({
    type: ADD_FOODS,
    payload: cartData
  });
};

export const openCart = () => dispatch => {
  dispatch({
    type: OPEN_CART,
    payload: null
  });
}

export const saveFoods = cartData => dispatch => {
  dispatch({
    type: SAVE_CART,
    payload: cartData
  });
};
