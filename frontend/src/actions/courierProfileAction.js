import {
    SET_ERRORS,
    SET_CURRENT_USER
} from "./types";
import axios from "axios";
import { api } from "utils";

const getCurrentCourierProfileSuccess = profile => {
    return {
        type: SET_CURRENT_USER,
        payload: profile
    };
}
 
const setError = error => {
    return {
        type: SET_ERRORS,
        payload: error
    }
}

export const getCurrentCourierProfile = () => dispatch => {
    axios
      .get(api.BASE_URL + api.GET_CURRENT_COURIER)
      .then(res => dispatch(getCurrentCourierProfileSuccess(res.data)))
      .catch(error => console.log(error));
}

export const handleChangeCourierImage = courierProfile => dispatch => {
    axios
      .put(api.BASE_URL + api.UPDATE_COURIER_PROFILE_PICTURE, courierProfile, {
        headers: {
          accept: "application/json",
          "Accept-Language": "en-US,en;q=0.8",
          "Content-Type": `multipart/form-data; boundary=${courierProfile._boundary}`
        }
      })
      .then(res => dispatch(getCurrentCourierProfile()))
      .catch(error => dispatch(setError(error.response.data)));
}

export const updateCourierProfile = courierProfile =>  dispatch => {
    axios
      .put(api.BASE_URL + api.UPDATE_COURIER_PROFILE, courierProfile)
      .then(res => dispatch(getCurrentCourierProfile(res.data)))
      .catch(error => dispatch(setError(error.response.data)));
}

export const deleteCourierProfile = () => dispatch => {
    // ignore it
}