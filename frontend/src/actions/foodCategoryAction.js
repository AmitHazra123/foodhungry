import axios from "axios";

import { api } from "../utils.js";
import {
  GET_ALL_FOOD_CATEGORY,
  ADD_FOOD_CATEGORY,
  DELETE_FOOD_CATEGORY,
  SET_ERRORS,
  UPDATE_FOOD_CATEGORY
} from "./types";

// actions success method
const getAllFoodCategorySuccess = foodCategories => {
  return {
    type: GET_ALL_FOOD_CATEGORY,
    payload: foodCategories
  };
};

const addFoodCategorySuccess = foodCategory => {
  return {
    type: ADD_FOOD_CATEGORY,
    payload: foodCategory
  };
};

const updateFoodCategorySuccess = foodCategory => {
  return {
    type: UPDATE_FOOD_CATEGORY,
    payload: foodCategory
  }
}

const deleteFoodCategorySuccess = foodId => {
  return {
    type: DELETE_FOOD_CATEGORY,
    payload: foodId
  };
};

const setErrorSuccess = errors => {
  return {
    type: SET_ERRORS,
    payload: errors
  };
};

// actions
// to get all food
export const getAllFoodCategory = () => dispatch => {
  axios
    .get(api.BASE_URL + api.GET_ALL_FOOD_CATEGORY)
    .then(res => dispatch(getAllFoodCategorySuccess(res.data)))
    .catch(error => dispatch(setErrorSuccess(error.response.data)));
};

// to add a food
export const addFoodCategory = foodCategory => dispatch => {
  axios
    .post(api.BASE_URL + api.ADD_FOOD_CATEGORY, foodCategory)
    .then(res => dispatch(addFoodCategorySuccess(res.data)))
    .catch(error => dispatch(setErrorSuccess(error.response.data)));
};

// to update a food
export const updateFoodCategory = (categoryId, foodCategory) => dispatch => {
  axios
    .put(api.BASE_URL + api.UPDATE_FOOD_CATEGORY + categoryId, foodCategory)
    .then(res => dispatch(updateFoodCategorySuccess(res.data)))
    .catch(error => dispatch(setErrorSuccess(error.response.data)));
}

// to delete a food
export const deleteFoodCategory = categoryId => dispatch => {
  axios
    .delete(api.BASE_URL + api.DELETE_FOOD_CATEGORY + categoryId)
    .then(res => {
      dispatch(deleteFoodCategorySuccess(categoryId));
    })
    .catch(error => dispatch(setErrorSuccess(error.response.data)));
};
