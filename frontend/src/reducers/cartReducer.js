import { ADD_FOODS, SAVE_CART, OPEN_CART } from '../actions/types';

const initialState = {
  open: false,
  foods: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ADD_FOODS:
      const food = state.foods.filter(food => food.foodId === action.payload.food.foodId)[0];
      if(food !== undefined) {
        // update food as it is already exists
        food.quantity ++;
        return {
          ...state,
          open: true,
          foods: Object.assign([], state.foods, food)
        };
      } else {
        // add new food as it is not exists
        return {
          ...state,
          open: true,
          foods: state.foods.concat(action.payload.food)
        };
      }

    case OPEN_CART:
      return {
        ...state,
        open: true
      }
    
    case SAVE_CART:
      return {
        open: false,
        foods: action.payload.foods
      };
    default:
      return state;
  }
}
