import { GET_ALL_FOOD, GET_FOOD, ADD_FOOD, UPDATE_FOOD, DELETE_FOOD } from "actions/types";

const initialState = {
  foods: [],
  food: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_FOOD:
      return {
        ...state,
        foods: action.payload
      }
    case GET_FOOD:
      return {
        ...state,
        food: action.payload
      }
    case ADD_FOOD:
      return {
        ...state,
        foods: [action.payload].concat(state)
      };
    case UPDATE_FOOD:
      return {
        ...state,
        foods: Object.assign([], state.foods, action.payload)
      };
    case DELETE_FOOD:
      return {
        ...state,
        foods: state.foods.filter(food => food._id !== action.payload)
      };
    default:
      return state;
  }
}
