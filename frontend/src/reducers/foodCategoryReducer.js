import {
  GET_ALL_FOOD_CATEGORY,
  ADD_FOOD_CATEGORY,
  UPDATE_FOOD_CATEGORY,
  DELETE_FOOD_CATEGORY
} from "actions/types";

const initialState = {};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ALL_FOOD_CATEGORY:
      return action.payload;
    case ADD_FOOD_CATEGORY:
      return [action.payload].concat(state);
    case UPDATE_FOOD_CATEGORY:
      return Object.assign([], state, [action.payload]);
    case DELETE_FOOD_CATEGORY:
      return state.filter(food => food._id !== action.payload);
    default:
      return state;
  }
}
