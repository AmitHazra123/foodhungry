import {SET_CURRENT_USER, DELETE_RESTAURANT_PROFILE, DELETE_USER_PROFILE} from '../actions/types';

const initialState = {
    isAuthenticate: false,
    user: {}
}

export default function(state = initialState, action) {
    switch(action.type) {
        case SET_CURRENT_USER:
            return {
                ...state,
                isAuthenticate: !(typeof action.payload === "object" && Object.keys(action.payload).length === 0),
                user: action.payload
            };
        case DELETE_RESTAURANT_PROFILE:
            return state;
        case DELETE_USER_PROFILE:
            return state;
        default:
            return state;
    }
}