import { combineReducers } from "redux";
import cartReducer from "./cartReducer";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import foodCategoryReducer from "./foodCategoryReducer";
import foodReducer from "./foodReducer";
import restaurantReducer from "./restaurantReducer";

export default combineReducers({
  cart: cartReducer,
  auth: authReducer,
  restaurantReducer,
  foodCategories: foodCategoryReducer,
  foodReducer,
  errors: errorReducer
});
