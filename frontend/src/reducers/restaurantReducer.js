import {SEARCH_RESTAURANTS, VIEW_RESTAURANTS_WITH_LOCATION, GET_RESTAURANT} from "../actions/types";

const initialState = {
    restaurants: [],
    restaurant: {}
};

export default function(state = initialState, action) {
    switch(action.type) {
        case SEARCH_RESTAURANTS:
            return state;
        case VIEW_RESTAURANTS_WITH_LOCATION:
            return {
                ...state,
                restaurants: action.payload
            }
        case GET_RESTAURANT:
            return {
                ...state,
                restaurant: action.payload
            }
        default:
            return state;
    }
}